pub const initialize = "initialize";
pub const initialized = "initialized";

pub const textDocument = enum {
    pub const didOpen = "textDocument/didOpen";
    pub const didChange = "textDocument/didChange";
    pub const didClose = "textDocument/didClose";
    pub const declaration = "textDocument/declaration";
};
