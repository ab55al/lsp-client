// version 3.17

const std = @import("std");
const core = @import("core");
pub const methods = @import("lsp_methods.zig");

pub const Error = error{ InvalidRequest, ParseError, MethodNotFound, InvalidParams, InternalError, UnknownServerError };
pub const errors = std.ComptimeStringMap(Error, .{
    .{ "InvalidRequest", Error.InvalidRequest },
    .{ "ParseError", Error.ParseError },
    .{ "MethodNotFound", Error.MethodNotFound },
    .{ "InvalidParams", Error.InvalidParams },
    .{ "InternalError", Error.InternalError },
});

pub const JsonTree = struct {
    raw: []const u8,
    parsed: std.json.Parsed(std.json.Value),

    pub fn deinit(self: *JsonTree, allocator: std.mem.Allocator) void {
        allocator.free(self.raw);
        self.parsed.deinit();
    }
};

pub fn Notification(comptime T: type) type {
    return struct {
        jsonrpc: []const u8 = "2.0",
        method: []const u8,
        params: T,
    };
}

pub fn Request(comptime T: type) type {
    return struct {
        jsonrpc: []const u8 = "2.0",
        id: union(enum) {
            integer: integer,
            string: []const u8,
            null,

            pub fn jsonStringify(self: @This(), options: std.json.StringifyOptions, out_stream: anytype) !void {
                try switch (self) {
                    .null => _ = try out_stream.write("null"),
                    inline else => |value| std.json.stringify(value, options, out_stream),
                };
            }
        },
        method: []const u8,
        params: T,
    };
}

pub fn Response(comptime T: type) type {
    return struct {
        jsonrpc: []const u8 = "2.0",
        id: union(enum) {
            integer: integer,
            string: []const u8,
            null,

            pub const jsonStringify = unionJsonStringify;
        },
        result: ?union(enum) { result: T, err: ResponseError },

        tree: JsonTree,
        arena: std.heap.ArenaAllocator,

        pub fn deinit(self: *@This(), allocator: std.mem.Allocator) void {
            self.tree.deinit(allocator);
            self.arena.deinit();
        }
    };
}

pub const ResponseError = struct {
    /// A number indicating the error type that occurred.
    code: integer,
    /// A string providing a short description of the error.
    message: []const u8,
    /// A primitive or structured value that contains additional
    /// information about the error. Can be omitted.
    data: std.json.Value = .null,
};

/// Creates a union that when stringified does not stringify the tag name
pub fn unionJsonStringify(self: anytype, options: std.json.StringifyOptions, out_stream: anytype) @TypeOf(out_stream).Error!void {
    const type_info = @typeInfo(@TypeOf(self));

    if (type_info == .Union) {
        try switch (self) {
            inline else => |value| std.json.stringify(value, options, out_stream),
        };
    }
    if (type_info == .Pointer and @typeInfo(type_info.Pointer.child) == .Union) {
        try switch (self.*) {
            inline else => |value| std.json.stringify(value, options, out_stream),
        };
    }
}

pub fn enumIntJsonStringify(self: anytype, options: std.json.StringifyOptions, out_stream: anytype) @TypeOf(out_stream).Error!void {
    _ = options;
    var copy = if (@typeInfo(@TypeOf(self)) == .Pointer) self.* else self;
    try out_stream.print("{}", .{@intFromEnum(copy)});
}

pub fn enumStringJsonStringify(self: anytype, options: std.json.StringifyOptions, out_stream: anytype) @TypeOf(out_stream).Error!void {
    _ = options;
    var copy = if (@typeInfo(@TypeOf(self)) == .Pointer) self.* else self;
    try out_stream.print("\"{s}\"", .{@tagName(copy)});
}

/// A special text edit with an additional change annotation.
pub const AnnotatedTextEdit = struct {
    /// The actual annotation identifier.
    annotationId: ChangeAnnotationIdentifier,
    /// The string to be inserted. For delete operations use an empty string.
    newText: []const u8,
    /// The range of the text document to be manipulated. To insert text into a document create a range where start === end.
    range: Range,
};

pub const ApplyWorkspaceEditParams = struct {
    /// The edits to apply.
    edit: WorkspaceEdit,
    /// An optional label of the workspace edit. This label is presented in the user interface for example on an undo stack to undo the workspace edit.
    label: []const u8,
};

pub const ApplyWorkspaceEditResult = struct {
    /// Indicates whether the edit was applied or not.
    applied: bool,
    /// Depending on the client's failure handling strategy `failedChange` might contain the index of the change that failed. This property is only available if the client signals a `failureHandling` strategy in its client capabilities.
    failedChange: uinteger,
    /// An optional textual description for why the edit was not applied. This may be used by the server for diagnostic logging or to provide a suitable error for a request that triggered the edit.
    failureReason: []const u8,
};

pub const CallHierarchyClientCapabilities = struct {
    /// Whether implementation supports dynamic registration. If this is set to `true` the client supports the new `(TextDocumentRegistrationOptions & StaticRegistrationOptions)` return value for the corresponding server capability as well.
    dynamicRegistration: bool = false,
};

pub const CallHierarchyIncomingCall = struct {
    /// The item that makes the call.
    from: CallHierarchyItem,
    /// The ranges at which the calls appear. This is relative to the caller denoted by [`this.from`](#CallHierarchyIncomingCall.from).
    fromRanges: []Range,
};

pub const CallHierarchyIncomingCallsParams = struct {
    item: CallHierarchyItem,
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const CallHierarchyItem = struct {
    /// A data entry field that is preserved between a call hierarchy prepare and incoming calls or outgoing calls requests.
    data: void,
    /// More detail for this item, e.g. the signature of a function.
    detail: []const u8,
    /// The kind of this item.
    kind: SymbolKind,
    /// The name of this item.
    name: []const u8,
    /// The range enclosing this symbol not including leading/trailing whitespace but everything else, e.g. comments and code.
    range: Range,
    /// The range that should be selected and revealed when this symbol is being picked, e.g. the name of a function. Must be contained by the [`range`](#CallHierarchyItem.range).
    selectionRange: Range,
    /// Tags for this item.
    tags: []SymbolTag,
    /// The resource identifier of this item.
    uri: DocumentUri,
};

pub const CallHierarchyOptions = struct {
    workDoneProgress: bool,
};

pub const CallHierarchyOutgoingCall = struct {
    /// The range at which this item is called. This is the range relative to the caller, e.g the item passed to `callHierarchy/outgoingCalls` request.
    fromRanges: []Range,
    /// The item that is called.
    to: CallHierarchyItem,
};

pub const CallHierarchyOutgoingCallsParams = struct {
    item: CallHierarchyItem,
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const CallHierarchyPrepareParams = struct {
    /// The position inside the text document.
    position: Position,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const CallHierarchyRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?DocumentSelector,
    /// The id used to register the request. The id can be used to deregister the request again. See also Registration#id.
    id: []const u8,
    workDoneProgress: bool,
};

pub const CancelParams = struct {
    /// The request id to cancel.
    id: union(enum) {
        integer: integer,
        string: []const u8,
    },
};

/// Additional information that describes document changes.
pub const ChangeAnnotation = struct {
    /// A human-readable string which is rendered less prominent in the user interface.
    description: []const u8,
    /// A human-readable string describing the actual change. The string is rendered prominent in the user interface.
    label: []const u8,
    /// A flag which indicates that user confirmation is needed before applying the change.
    needsConfirmation: bool,
};

/// An identifier referring to a change annotation managed by a workspace edit.
pub const ChangeAnnotationIdentifier = []const u8;

pub const ClientCapabilities = struct {
    /// Experimental client capabilities.
    experimental: LSPAny = .null,
    /// General client capabilities.
    general: struct {
        /// Client capabilities specific to the client's markdown parser.
        markdown: ?MarkdownClientCapabilities = null,
        /// The position encodings supported by the client. Client and server have to agree on the same position encoding to ensure that offsets (e.g. character position in a line) are interpreted the same on both side.
        ///
        /// To keep the protocol backwards compatible the following applies: if the value 'utf-16' is missing from the array of position encodings servers can assume that the client supports UTF-16. UTF-16 is therefore a mandatory encoding.
        ///
        /// If omitted it defaults to ['utf-16'].
        ///
        /// Implementation considerations: since the conversion from one encoding into another requires the content of the file / line the conversion is best done where the file is read which is usually on the server side.
        positionEncodings: []PositionEncodingKind = @constCast(&[_][]const u8{"utf-8"}),
        /// Client capabilities specific to regular expressions.
        regularExpressions: ?RegularExpressionsClientCapabilities = null,
        /// Client capability that signals how the client handles stale requests (e.g. a request for which the client will not process the response anymore since the information is outdated).
        staleRequestSupport: struct {
            /// The client will actively cancel the request.
            cancel: bool = false,
            /// The list of requests for which the client will retry the request if it receives a response with error code `ContentModified``
            retryOnContentModified: [][]const u8 = &.{},
        } = .{},
    } = .{},
    /// Capabilities specific to the notebook document support.
    notebookDocument: ?NotebookDocumentClientCapabilities = null,
    /// Text document specific client capabilities.
    textDocument: ?TextDocumentClientCapabilities = .{},
    /// Window specific client capabilities.
    window: ?struct {
        /// Client capabilities for the show document request.
        showDocument: ShowDocumentClientCapabilities,
        /// Capabilities specific to the showMessage request
        showMessage: ShowMessageRequestClientCapabilities,
        /// It indicates whether the client supports server initiated progress using the `window/workDoneProgress/create` request.
        ///
        /// The capability also controls Whether client supports handling of progress notifications. If set servers are allowed to report a `workDoneProgress` property in the request specific server capabilities.
        workDoneProgress: bool,
    } = null,
    /// Workspace specific client capabilities.
    workspace: ?struct {
        /// The client supports applying batch edits to the workspace by supporting the request 'workspace/applyEdit'
        applyEdit: ?bool = null,
        /// Capabilities specific to the code lens requests scoped to the workspace.
        codeLens: ?CodeLensWorkspaceClientCapabilities = null,
        /// The client supports `workspace/configuration` requests.
        configuration: ?bool = null,
        /// Client workspace capabilities specific to diagnostics.
        diagnostics: ?DiagnosticWorkspaceClientCapabilities = null,
        /// Capabilities specific to the `workspace/didChangeConfiguration` notification.
        didChangeConfiguration: ?DidChangeConfigurationClientCapabilities = null,
        /// Capabilities specific to the `workspace/didChangeWatchedFiles` notification.
        didChangeWatchedFiles: ?DidChangeWatchedFilesClientCapabilities = null,
        /// Capabilities specific to the `workspace/executeCommand` request.
        executeCommand: ?ExecuteCommandClientCapabilities = null,
        /// The client has support for file requests/notifications.
        fileOperations: struct {
            /// The client has support for sending didCreateFiles notifications.
            didCreate: ?bool = true,
            /// The client has support for sending didDeleteFiles notifications.
            didDelete: ?bool = true,
            /// The client has support for sending didRenameFiles notifications.
            didRename: ?bool = true,
            /// The client has support for sending willCreateFiles requests.
            willCreate: ?bool = true,
            /// The client has support for sending willDeleteFiles requests.
            willDelete: ?bool = true,
            /// The client has support for sending willRenameFiles requests.
            willRename: ?bool = true,
            /// Whether the client supports dynamic registration for file requests/notifications.
            dynamicRegistration: ?bool = null,
        } = .{},
        /// Client workspace capabilities specific to inlay hints.
        inlayHint: ?InlayHintWorkspaceClientCapabilities = null,
        /// Client workspace capabilities specific to inline values.
        inlineValue: ?InlineValueWorkspaceClientCapabilities = null,
        /// Capabilities specific to the semantic token requests scoped to the workspace.
        semanticTokens: ?SemanticTokensWorkspaceClientCapabilities = null,
        /// Capabilities specific to the `workspace/symbol` request.
        symbol: ?WorkspaceSymbolClientCapabilities = null,
        /// Capabilities specific to `WorkspaceEdit`s
        workspaceEdit: ?WorkspaceEditClientCapabilities = null,
        /// The client has support for workspace folders.
        workspaceFolders: ?bool = null,
    } = .{},
};

/// * A code action represents a change that can be performed in code, e.g. to fix  * a problem or to refactor code.   *    * A CodeAction must set either `edit` and/or a `command`. If both are supplied,     * the `edit` is applied first, then the `command` is executed.
pub const CodeAction = struct {
    /// * A command this code action executes. If a code action   * provides an edit and a command, first the edit is     * executed and then the command.
    command: Command,
    /// * A data entry field that is preserved on a code action between   * a `textDocument/codeAction` and a `codeAction/resolve` request.     *       * @since 3.16.0
    data: LSPAny,
    /// * The diagnostics that this code action resolves.
    diagnostics: []Diagnostic,
    /// * Marks that the code action cannot currently be applied.   *     * Clients should follow the following guidelines regarding disabled code       * actions:         *           * - Disabled code actions are not shown in automatic lightbulbs code             *   action menus.               *                 * - Disabled actions are shown as faded out in the code action menu when                    *   the user request a more specific type of code action, such as                      *   refactorings.                        *                          * - If the user has a keybinding that auto applies a code action and only                            *   a disabled code actions are returned, the client should show the user                              *   an error message with `reason` in the editor.                                *                                  * @since 3.16.0
    disabled: struct {
        /// * Human readable description of why the code action is currently     * disabled.         *             * This is displayed in the code actions UI.
        reason: []const u8,
    },
    /// * The workspace edit this code action performs.
    edit: WorkspaceEdit,
    /// * Marks this as a preferred action. Preferred actions are used by the    * `auto fix` command and can be targeted by keybindings.      *        * A quick fix should be marked preferred if it properly addresses the          * underlying error. A refactoring should be marked preferred if it is the            * most reasonable choice of actions to take.              *                * @since 3.15.0
    isPreferred: bool,
    /// * The kind of the code action.   *     * Used to filter code actions.
    kind: CodeActionKind,
    /// * A short, human-readable, title for this code action.
    title: []const u8,
};

pub const CodeActionClientCapabilities = struct {
    /// The client supports code action literals as a valid response of the `textDocument/codeAction` request.
    codeActionLiteralSupport: struct {
        /// The code action kind is supported with the following value set.
        codeActionKind: struct {
            /// The code action kind values the client supports. When this property exists the client also guarantees that it will handle values outside its set gracefully and falls back to a default value when unknown.
            valueSet: []CodeActionKind,
        },
    },
    /// Whether code action supports the `data` property which is preserved between a `textDocument/codeAction` and a `codeAction/resolve` request.
    dataSupport: bool,
    /// Whether code action supports the `disabled` property.
    disabledSupport: bool,
    /// Whether code action supports dynamic registration.
    dynamicRegistration: bool,
    /// Whether the client honors the change annotations in text edits and resource operations returned via the `CodeAction#edit` property by for example presenting the workspace edit in the user interface and asking for confirmation.
    honorsChangeAnnotations: bool,
    /// Whether code action supports the `isPreferred` property.
    isPreferredSupport: bool,
    /// Whether the client supports resolving additional code action properties via a separate `codeAction/resolve` request.
    resolveSupport: struct {
        /// The properties that a client can resolve lazily.
        properties: [][]const u8,
    },
};

/// * Contains additional diagnostic information about the context in which  * a code action is run.
pub const CodeActionContext = struct {
    /// * An array of diagnostics known on the client side overlapping the range   * provided to the `textDocument/codeAction` request. They are provided so     * that the server knows which errors are currently presented to the user       * for the given range. There is no guarantee that these accurately reflect         * the error state of the resource. The primary parameter           * to compute code actions is the provided range.
    diagnostics: []Diagnostic,
    /// * Requested kind of actions to return.   *     * Actions not of this kind are filtered out by the client before being       * shown. So servers can omit computing them.
    only: []CodeActionKind,
    /// * The reason why code actions were requested.   *     * @since 3.17.0
    triggerKind: CodeActionTriggerKind,
};

/// * The kind of a code action.  *   * Kinds are a hierarchical list of identifiers separated by `.`,    * e.g. `"refactor.extract.function"`.     *      * The set of kinds is open and client needs to announce the kinds it supports       * to the server during initialization.
///  * A set of predefined code action kinds.
pub const CodeActionKind = []const u8;

pub const CodeActionOptions = struct {
    /// * CodeActionKinds that this server may return.   *      * The list of kinds may be generic, such as `CodeActionKind.Refactor`,        * or the server may list out every specific kind they provide.
    codeActionKinds: []CodeActionKind,
    /// * The server provides support to resolve additional   * information for a code action.     *       * @since 3.16.0
    resolveProvider: bool,
    workDoneProgress: bool,
};

/// * Params for the CodeActionRequest
pub const CodeActionParams = struct {
    /// * Context carrying additional information.
    context: CodeActionContext,
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// * The range for which the command was invoked.
    range: Range,
    /// * The document in which the command was invoked.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const CodeActionRegistrationOptions = struct {
    /// * CodeActionKinds that this server may return.   *      * The list of kinds may be generic, such as `CodeActionKind.Refactor`,        * or the server may list out every specific kind they provide.
    codeActionKinds: []CodeActionKind,
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    /// * The server provides support to resolve additional   * information for a code action.     *       * @since 3.16.0
    resolveProvider: bool,
    workDoneProgress: bool,
};

/// * The reason why code actions were requested.  *   * @since 3.17.0
pub const CodeActionTriggerKind = enum(u16) {
    Invoked = 1,
    Automatic = 2,
};

/// Structure to capture a description for an error code.
pub const CodeDescription = struct {
    /// An URI to open with more information about the diagnostic error.
    href: URI,
};

/// A code lens represents a command that should be shown along with source text, like the number of references, a way to run tests, etc.
///
/// A code lens is _unresolved_ when no command is associated to it. For performance reasons the creation of a code lens and resolving should be done in two stages.
pub const CodeLens = struct {
    /// The command this code lens represents.
    command: Command,
    /// A data entry field that is preserved on a code lens item between a code lens and a code lens resolve request.
    data: LSPAny,
    /// The range in which this code lens is valid. Should only span a single line.
    range: Range,
};

pub const CodeLensClientCapabilities = struct {
    /// Whether code lens supports dynamic registration.
    dynamicRegistration: bool,
};

pub const CodeLensOptions = struct {
    /// Code lens has a resolve provider as well.
    resolveProvider: bool,
    workDoneProgress: bool,
};

pub const CodeLensParams = struct {
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// The document to request code lens for.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const CodeLensRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    /// Code lens has a resolve provider as well.
    resolveProvider: bool,
    workDoneProgress: bool,
};

pub const CodeLensWorkspaceClientCapabilities = struct {
    /// Whether the client implementation supports a refresh request sent from the server to the client.
    ///
    /// Note that this event is global and will force the client to refresh all code lenses currently shown. It should be used with absolute care and is useful for situation where a server for example detect a project wide change that requires such a calculation.
    refreshSupport: bool,
};

/// * Represents a color in RGBA space.
pub const Color = struct {
    /// * The alpha component of this color in the range [0-1].
    alpha: decimal,
    /// * The blue component of this color in the range [0-1].
    blue: decimal,
    /// * The green component of this color in the range [0-1].
    green: decimal,
    /// * The red component of this color in the range [0-1].
    red: decimal,
};

pub const ColorInformation = struct {
    /// * The actual color value for this color range.
    color: Color,
    /// * The range in the document where this color appears.
    range: Range,
};

pub const ColorPresentation = struct {
    /// * An optional array of additional [text edits](#TextEdit) that are applied    * when selecting this color presentation. Edits must not overlap with the      * main [edit](#ColorPresentation.textEdit) nor with themselves.
    additionalTextEdits: []TextEdit,
    /// * The label of this color presentation. It will be shown on the color   * picker header. By default this is also the text that is inserted when     * selecting this color presentation.
    label: []const u8,
    /// * An [edit](#TextEdit) which is applied to a document when selecting   * this presentation for the color. When omitted the      * [label](#ColorPresentation.label) is used.
    textEdit: TextEdit,
};

pub const ColorPresentationParams = struct {
    /// * The color information to request presentations for.
    color: Color,
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// * The range where the color would be inserted. Serves as a context.
    range: Range,
    /// * The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const Command = struct {
    /// Arguments that the command handler should be invoked with.
    arguments: []LSPAny,
    /// The identifier of the actual command handler.
    command: []const u8,
    /// Title of the command, like `save`.
    title: []const u8,
};

pub const CompletionClientCapabilities = struct {
    /// The client supports the following `CompletionItem` specific capabilities.
    completionItem: struct {
        /// Client supports commit characters on a completion item.
        commitCharactersSupport: bool,
        /// Client supports the deprecated property on a completion item.
        deprecatedSupport: bool,
        /// Client supports the follow content formats for the documentation property. The order describes the preferred format of the client.
        documentationFormat: []MarkupKind,
        /// Client supports insert replace edit to control different behavior if a completion item is inserted in the text or should replace text.
        insertReplaceSupport: bool,
        /// The client supports the `insertTextMode` property on a completion item to override the whitespace handling mode as defined by the client (see `insertTextMode`).
        insertTextModeSupport: struct {
            valueSet: []InsertTextMode,
        },
        /// The client has support for completion item label details (see also `CompletionItemLabelDetails`).
        labelDetailsSupport: bool,
        /// Client supports the preselect property on a completion item.
        preselectSupport: bool,
        /// Indicates which properties a client can resolve lazily on a completion item. Before version 3.16.0 only the predefined properties `documentation` and `detail` could be resolved lazily.
        resolveSupport: struct {
            /// The properties that a client can resolve lazily.
            properties: [][]const u8,
        },
        /// Client supports snippets as insert text.
        ///
        /// A snippet can define tab stops and placeholders with `$1`, `$2` and `${3:foo}`. `$0` defines the final tab stop, it defaults to the end of the snippet. Placeholders with equal identifiers are linked, that is typing in one will update others too.
        snippetSupport: bool,
        /// Client supports the tag property on a completion item. Clients supporting tags have to handle unknown tags gracefully. Clients especially need to preserve unknown tags when sending a completion item back to the server in a resolve call.
        tagSupport: struct {
            /// The tags supported by the client.
            valueSet: []CompletionItemTag,
        },
    },
    completionItemKind: struct {
        /// The completion item kind values the client supports. When this property exists the client also guarantees that it will handle values outside its set gracefully and falls back to a default value when unknown.
        ///
        /// If this property is not present the client only supports the completion items kinds from `Text` to `Reference` as defined in the initial version of the protocol.
        valueSet: []CompletionItemKind,
    },
    /// The client supports the following `CompletionList` specific capabilities.
    completionList: struct {
        /// The client supports the following itemDefaults on a completion list.
        ///
        /// The value lists the supported property names of the `CompletionList.itemDefaults` object. If omitted no properties are supported.
        itemDefaults: [][]const u8,
    },
    /// The client supports to send additional context information for a `textDocument/completion` request.
    contextSupport: bool,
    /// Whether completion supports dynamic registration.
    dynamicRegistration: bool,
    /// The client's default when the completion item doesn't provide a `insertTextMode` property.
    insertTextMode: InsertTextMode,
};

/// Contains additional information about the context in which a completion request is triggered.
pub const CompletionContext = struct {
    /// The trigger character (a single character) that has trigger code complete. Is undefined if `triggerKind !== CompletionTriggerKind.TriggerCharacter`
    triggerCharacter: []const u8,
    /// How the completion was triggered.
    triggerKind: CompletionTriggerKind,
};

pub const CompletionItem = struct {
    /// An optional array of additional text edits that are applied when selecting this completion. Edits must not overlap (including the same insert position) with the main edit nor with themselves.
    ///
    /// Additional text edits should be used to change text unrelated to the current cursor position (for example adding an import statement at the top of the file if the completion item will insert an unqualified type).
    additionalTextEdits: []TextEdit,
    /// An optional command that is executed *after* inserting this completion.
    /// *Note* that additional modifications to the current document should be described with the additionalTextEdits-property.
    command: Command,
    /// An optional set of characters that when pressed while this completion is active will accept it first and then type that character. *Note* that all commit characters should have `length=1` and that superfluous characters will be ignored.
    commitCharacters: [][]const u8,
    /// A data entry field that is preserved on a completion item between a completion and a completion resolve request.
    data: LSPAny,
    /// Indicates if this item is deprecated.
    deprecated: bool,
    /// A human-readable string with additional information about this item, like type or symbol information.
    detail: []const u8,
    /// A human-readable string that represents a doc-comment.
    documentation: union(enum) {
        modify_0: []const u8,
        modify_1: MarkupContent,
    },
    /// A string that should be used when filtering a set of completion items. When omitted the label is used as the filter text for this item.
    filterText: []const u8,
    /// A string that should be inserted into a document when selecting this completion. When omitted the label is used as the insert text for this item.
    ///
    /// The `insertText` is subject to interpretation by the client side. Some tools might not take the string literally. For example VS Code when code complete is requested in this example `con<cursor position>` and a completion item with an `insertText` of `console` is provided it will only insert `sole`. Therefore it is recommended to use `textEdit` instead since it avoids additional client side interpretation.
    insertText: []const u8,
    /// The format of the insert text. The format applies to both the `insertText` property and the `newText` property of a provided `textEdit`. If omitted defaults to `InsertTextFormat.PlainText`.
    ///
    /// Please note that the insertTextFormat doesn't apply to `additionalTextEdits`.
    insertTextFormat: InsertTextFormat,
    /// How whitespace and indentation is handled during completion item insertion. If not provided the client's default value depends on the `textDocument.completion.insertTextMode` client capability.
    insertTextMode: InsertTextMode,
    /// The kind of this completion item. Based of the kind an icon is chosen by the editor. The standardized set of available values is defined in `CompletionItemKind`.
    kind: CompletionItemKind,
    /// The label of this completion item.
    ///
    /// The label property is also by default the text that is inserted when selecting this completion.
    ///
    /// If label details are provided the label itself should be an unqualified name of the completion item.
    label: []const u8,
    /// Additional details for the label
    labelDetails: CompletionItemLabelDetails,
    /// Select this item when showing.
    ///
    /// *Note* that only one completion item can be selected and that the tool / client decides which item that is. The rule is that the *first* item of those that match best is selected.
    preselect: bool,
    /// A string that should be used when comparing this item with other items. When omitted the label is used as the sort text for this item.
    sortText: []const u8,
    /// Tags for this completion item.
    tags: []CompletionItemTag,
    /// An edit which is applied to a document when selecting this completion. When an edit is provided the value of `insertText` is ignored.
    ///
    /// *Note:* The range of the edit must be a single line range and it must contain the position at which completion has been requested.
    ///
    /// Most editors support two different operations when accepting a completion item. One is to insert a completion text and the other is to replace an existing text with a completion text. Since this can usually not be predetermined by a server it can report both ranges. Clients need to signal support for `InsertReplaceEdit`s via the `textDocument.completion.completionItem.insertReplaceSupport` client capability property.
    ///
    /// *Note 1:* The text edit's range as well as both ranges from an insert replace edit must be a [single line] and they must contain the position at which completion has been requested.
    /// *Note 2:* If an `InsertReplaceEdit` is returned the edit's insert range must be a prefix of the edit's replace range, that means it must be contained and starting at the same position.
    textEdit: union(enum) {
        modify_0: TextEdit,
        modify_1: InsertReplaceEdit,
    },
    /// The edit text used if the completion item is part of a CompletionList and CompletionList defines an item default for the text edit range.
    ///
    /// Clients will only honor this property if they opt into completion list item defaults using the capability `completionList.itemDefaults`.
    ///
    /// If not provided and a list's default range is provided the label property is used as a text.
    textEditText: []const u8,
};

/// The kind of a completion entry.
pub const CompletionItemKind = enum(u16) {
    Text = 1,
    Method = 2,
    Function = 3,
    Constructor = 4,
    Field = 5,
    Variable = 6,
    Class = 7,
    Interface = 8,
    Module = 9,
    Property = 10,
    Unit = 11,
    Value = 12,
    Enum = 13,
    Keyword = 14,
    Snippet = 15,
    Color = 16,
    File = 17,
    Reference = 18,
    Folder = 19,
    EnumMember = 20,
    Constant = 21,
    Struct = 22,
    Event = 23,
    Operator = 24,
    TypeParameter = 25,

    pub const jsonStringify = enumIntJsonStringify;
};

/// Additional details for a completion item label.
pub const CompletionItemLabelDetails = struct {
    /// An optional string which is rendered less prominently after
    ///  {@link  CompletionItemLabelDetails.detail } . Should be used for fully qualified names or file path.
    description: []const u8,
    /// An optional string which is rendered less prominently directly after
    ///  {@link  CompletionItem.label label } , without any spacing. Should be used for function signatures or type annotations.
    detail: []const u8,
};

/// Completion item tags are extra annotations that tweak the rendering of a completion item.
pub const CompletionItemTag = []const u8;

/// Represents a collection of [completion items](#CompletionItem) to be presented in the editor.
pub const CompletionList = struct {
    /// This list is not complete. Further typing should result in recomputing this list.
    ///
    /// Recomputed lists have all their items replaced (not appended) in the incomplete completion sessions.
    isIncomplete: bool,
    /// In many cases the items of an actual completion result share the same value for properties like `commitCharacters` or the range of a text edit. A completion list can therefore define item defaults which will be used if a completion item itself doesn't specify the value.
    ///
    /// If a completion list specifies a default value and a completion item also specifies a corresponding value the one from the item is used.
    ///
    /// Servers are only allowed to return default values if the client signals support for this via the `completionList.itemDefaults` capability.
    itemDefaults: struct {
        /// A default commit character set.
        commitCharacters: [][]const u8,
        /// A default data value.
        data: LSPAny,
        /// A default edit range
        editRange: union(enum) {
            modify_0: Range,
            modify_1: struct {
                insert: Range,
                replace: Range,
            },
        },
        /// A default insert text format
        insertTextFormat: InsertTextFormat,
        /// A default insert text mode
        insertTextMode: InsertTextMode,
    },
    /// The completion items.
    items: []CompletionItem,
};

/// Completion options.
pub const CompletionOptions = struct {
    /// The list of all possible characters that commit a completion. This field can be used if clients don't support individual commit characters per completion item. See client capability `completion.completionItem.commitCharactersSupport`.
    ///
    /// If a server provides both `allCommitCharacters` and commit characters on an individual completion item the ones on the completion item win.
    allCommitCharacters: [][]const u8 = &.{},
    /// The server supports the following `CompletionItem` specific capabilities.
    completionItem: ?struct {
        /// The server has support for completion item label details (see also `CompletionItemLabelDetails`) when receiving a completion item in a resolve call.
        labelDetailsSupport: bool,
    } = null,
    /// The server provides support to resolve additional information for a completion item.
    resolveProvider: bool,
    /// The additional characters, beyond the defaults provided by the client (typically [a-zA-Z]), that should automatically trigger a completion request. For example `.` in JavaScript represents the beginning of an object property or method and is thus a good candidate for triggering a completion request.
    ///
    /// Most tools trigger a completion request automatically without explicitly requesting it using a keyboard shortcut (e.g. Ctrl+Space). Typically they do so when the user starts to type an identifier. For example if the user types `c` in a JavaScript file code complete will automatically pop up present `console` besides others as a completion item. Characters that make up identifiers don't need to be listed here.
    triggerCharacters: [][]const u8,
    workDoneProgress: ?bool = null,
};

pub const CompletionParams = struct {
    /// The completion context. This is only available if the client specifies to send this using the client capability `completion.contextSupport === true`
    context: CompletionContext,
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// The position inside the text document.
    position: Position,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const CompletionRegistrationOptions = struct {
    /// The list of all possible characters that commit a completion. This field can be used if clients don't support individual commit characters per completion item. See client capability `completion.completionItem.commitCharactersSupport`.
    ///
    /// If a server provides both `allCommitCharacters` and commit characters on an individual completion item the ones on the completion item win.
    allCommitCharacters: [][]const u8,
    /// The server supports the following `CompletionItem` specific capabilities.
    completionItem: struct {
        /// The server has support for completion item label details (see also `CompletionItemLabelDetails`) when receiving a completion item in a resolve call.
        labelDetailsSupport: bool,
    },
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    /// The server provides support to resolve additional information for a completion item.
    resolveProvider: bool,
    /// The additional characters, beyond the defaults provided by the client (typically [a-zA-Z]), that should automatically trigger a completion request. For example `.` in JavaScript represents the beginning of an object property or method and is thus a good candidate for triggering a completion request.
    ///
    /// Most tools trigger a completion request automatically without explicitly requesting it using a keyboard shortcut (e.g. Ctrl+Space). Typically they do so when the user starts to type an identifier. For example if the user types `c` in a JavaScript file code complete will automatically pop up present `console` besides others as a completion item. Characters that make up identifiers don't need to be listed here.
    triggerCharacters: [][]const u8,
    workDoneProgress: bool,
};

/// How a completion was triggered
pub const CompletionTriggerKind = enum(u16) {
    Invoked = 1,
    TriggerCharacter = 2,
    TriggerForIncompleteCompletions = 3,
};

pub const ConfigurationItem = struct {
    /// * The scope to get the configuration section for.
    scopeUri: DocumentUri,
    /// * The configuration section asked for.
    section: []const u8,
};

pub const ConfigurationParams = struct {
    items: []ConfigurationItem,
};

/// Create file operation
pub const CreateFile = struct {
    /// An optional annotation identifier describing the operation.
    annotationId: ChangeAnnotationIdentifier,
    /// A create
    kind: []const u8,
    /// Additional options
    options: CreateFileOptions,
    /// The resource to create.
    uri: DocumentUri,
};

/// Options to create a file.
pub const CreateFileOptions = struct {
    /// Ignore if exists.
    ignoreIfExists: bool,
    /// Overwrite existing file. Overwrite wins over `ignoreIfExists`
    overwrite: bool,
};

/// * The parameters sent in notifications/requests for user-initiated creation  * of files.   *    * @since 3.16.0
pub const CreateFilesParams = struct {
    /// * An array of all files/folders created in this operation.
    files: []FileCreate,
};

pub const DeclarationClientCapabilities = struct {
    /// Whether declaration supports dynamic registration. If this is set to `true` the client supports the new
    /// `DeclarationRegistrationOptions` return value for the corresponding server capability as well.
    dynamicRegistration: bool = true,
    /// The client supports additional metadata in the form of declaration links.
    linkSupport: bool = true,
};

pub const DeclarationOptions = struct {
    workDoneProgress: bool,
};

pub const DeclarationParams = struct {
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ?ProgressToken = null,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ?ProgressToken = null,
    /// The position inside the text document.
    position: Position,
    /// The text document.
    textDocument: TextDocumentIdentifier,

    pub const Response = union(enum) {
        result: ?union(enum) {
            loc: Location,
            locs: []Location,
            loc_links: []LocationLink,
        },

        part_result: ?union(enum) {
            locs: []Location,
            loc_links: []LocationLink,
        },
    };
};

pub const DeclarationRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    /// The id used to register the request. The id can be used to deregister the request again. See also Registration#id.
    id: []const u8,
    workDoneProgress: bool,
};

pub const DefinitionClientCapabilities = struct {
    /// Whether definition supports dynamic registration.
    dynamicRegistration: bool,
    /// The client supports additional metadata in the form of definition links.
    linkSupport: bool,
};

pub const DefinitionOptions = struct {
    workDoneProgress: bool,
};

pub const DefinitionParams = struct {
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// The position inside the text document.
    position: Position,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const DefinitionRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    workDoneProgress: bool,
};

/// Delete file operation
pub const DeleteFile = struct {
    /// An optional annotation identifier describing the operation.
    annotationId: ChangeAnnotationIdentifier,
    /// A delete
    kind: []const u8,
    /// Delete options.
    options: DeleteFileOptions,
    /// The file to delete.
    uri: DocumentUri,
};

/// Delete file options
pub const DeleteFileOptions = struct {
    /// Ignore the operation if the file doesn't exist.
    ignoreIfNotExists: bool,
    /// Delete the content recursively if a folder is denoted.
    recursive: bool,
};

/// * The parameters sent in notifications/requests for user-initiated deletes  * of files.   *    * @since 3.16.0
pub const DeleteFilesParams = struct {
    /// * An array of all files/folders deleted in this operation.
    files: []FileDelete,
};

pub const Diagnostic = struct {
    /// The diagnostic's code, which might appear in the user interface.
    code: union(enum) {
        modify_0: integer,
        modify_1: []const u8,
    },
    /// An optional property to describe the error code.
    codeDescription: CodeDescription,
    /// A data entry field that is preserved between a `textDocument/publishDiagnostics` notification and `textDocument/codeAction` request.
    data: void,
    /// The diagnostic's message.
    message: []const u8,
    /// The range at which the message applies.
    range: Range,
    /// An array of related diagnostic information, e.g. when symbol-names within a scope collide all definitions can be marked via this property.
    relatedInformation: []DiagnosticRelatedInformation,
    /// The diagnostic's severity. Can be omitted. If omitted it is up to the client to interpret diagnostics as error, warning, info or hint.
    severity: DiagnosticSeverity,
    /// A human-readable string describing the source of this diagnostic, e.g. 'typescript' or 'super lint'.
    source: []const u8,
    /// Additional metadata about the diagnostic.
    tags: []DiagnosticTag,
};

/// Client capabilities specific to diagnostic pull requests.
pub const DiagnosticClientCapabilities = struct {
    /// Whether implementation supports dynamic registration. If this is set to `true` the client supports the new `(TextDocumentRegistrationOptions & StaticRegistrationOptions)` return value for the corresponding server capability as well.
    dynamicRegistration: bool,
    /// Whether the clients supports related documents for document diagnostic pulls.
    relatedDocumentSupport: bool,
};

/// Diagnostic options.
pub const DiagnosticOptions = struct {
    /// An optional identifier under which the diagnostics are managed by the client.
    identifier: []const u8,
    /// Whether the language has inter file dependencies meaning that editing code in one file can result in a different diagnostic set in another file. Inter file dependencies are common for most programming languages and typically uncommon for linters.
    interFileDependencies: bool,
    workDoneProgress: bool,
    /// The server provides support for workspace diagnostics as well.
    workspaceDiagnostics: bool,
};

/// Diagnostic registration options.
pub const DiagnosticRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    /// The id used to register the request. The id can be used to deregister the request again. See also Registration#id.
    id: []const u8,
    /// An optional identifier under which the diagnostics are managed by the client.
    identifier: []const u8,
    /// Whether the language has inter file dependencies meaning that editing code in one file can result in a different diagnostic set in another file. Inter file dependencies are common for most programming languages and typically uncommon for linters.
    interFileDependencies: bool,
    workDoneProgress: bool,
    /// The server provides support for workspace diagnostics as well.
    workspaceDiagnostics: bool,
};

/// Represents a related message and source code location for a diagnostic. This should be used to point to code locations that cause or are related to a diagnostics, e.g when duplicating a symbol in a scope.
pub const DiagnosticRelatedInformation = struct {
    /// The location of this related diagnostic information.
    location: Location,
    /// The message of this related diagnostic information.
    message: []const u8,
};

/// Cancellation data returned from a diagnostic request.
pub const DiagnosticServerCancellationData = struct {
    retriggerRequest: bool,
};

pub const DiagnosticSeverity = enum(u16) {
    Error = 1,
    Warning = 2,
    Information = 3,
    Hint = 4,
};

/// The diagnostic tags.
pub const DiagnosticTag = enum(u16) {
    Unnecessary = 1,
    Deprecated = 2,

    pub const jsonStringify = enumIntJsonStringify;
};

/// Workspace client capabilities specific to diagnostic pull requests.
pub const DiagnosticWorkspaceClientCapabilities = struct {
    /// Whether the client implementation supports a refresh request sent from the server to the client.
    ///
    /// Note that this event is global and will force the client to refresh all pulled diagnostics currently shown. It should be used with absolute care and is useful for situation where a server for example detects a project wide change that requires such a calculation.
    refreshSupport: bool,
};

pub const DidChangeConfigurationClientCapabilities = struct {
    /// * Did change configuration notification supports dynamic registration.
    dynamicRegistration: bool,
};

pub const DidChangeConfigurationParams = struct {
    /// * The actual changed settings
    settings: LSPAny,
};

/// The params sent in a change notebook document notification.
pub const DidChangeNotebookDocumentParams = struct {
    /// The actual changes to the notebook document.
    ///
    /// The change describes single state change to the notebook document. So it moves a notebook document, its cells and its cell text document contents from state S to S'.
    ///
    /// To mirror the content of a notebook using change events use the following approach:
    /// - start with the same initial content
    /// - apply the 'notebookDocument/didChange' notifications in the order   you receive them.
    change: NotebookDocumentChangeEvent,
    /// The notebook document that did change. The version number points to the version after all provided changes have been applied.
    notebookDocument: VersionedNotebookDocumentIdentifier,
};

pub const DidChangeTextDocumentParams = struct {
    /// The actual content changes. The content changes describe single state changes to the document. So if there are two content changes c1 (at array index 0) and c2 (at array index 1) for a document in state S then c1 moves the document from S to S' and c2 from S' to S''. So c1 is computed on the state S and c2 is computed on the state S'.
    ///
    /// To mirror the content of a document using change events use the following approach:
    /// - start with the same initial content
    /// - apply the 'textDocument/didChange' notifications in the order you   receive them.
    /// - apply the `TextDocumentContentChangeEvent`s in a single notification   in the order you receive them.
    contentChanges: []const TextDocumentContentChangeEvent,
    /// The document that did change. The version number points to the version after all provided content changes have been applied.
    textDocument: VersionedTextDocumentIdentifier,
};

pub const DidChangeWatchedFilesClientCapabilities = struct {
    /// * Did change watched files notification supports dynamic registration.    * Please note that the current protocol doesn't support static      * configuration for file changes from the server side.
    dynamicRegistration: bool,
    /// * Whether the client has support for relative patterns   * or not.      *        * @since 3.17.0
    relativePatternSupport: bool,
};

pub const DidChangeWatchedFilesParams = struct {
    /// The actual file events.
    changes: []FileEvent,
};

/// * Describe options to be used when registering for file system change events.
pub const DidChangeWatchedFilesRegistrationOptions = struct {
    /// * The watchers to register.
    watchers: []FileSystemWatcher,
};

pub const DidChangeWorkspaceFoldersParams = struct {
    /// * The actual workspace folder change event.
    event: WorkspaceFoldersChangeEvent,
};

/// The params sent in a close notebook document notification.
pub const DidCloseNotebookDocumentParams = struct {
    /// The text documents that represent the content of a notebook cell that got closed.
    cellTextDocuments: []TextDocumentIdentifier,
    /// The notebook document that got closed.
    notebookDocument: NotebookDocumentIdentifier,
};

pub const DidCloseTextDocumentParams = struct {
    /// The document that was closed.
    textDocument: TextDocumentIdentifier,
};

/// The params sent in an open notebook document notification.
pub const DidOpenNotebookDocumentParams = struct {
    /// The text documents that represent the content of a notebook cell.
    cellTextDocuments: []TextDocumentItem,
    /// The notebook document that got opened.
    notebookDocument: NotebookDocument,
};

pub const DidOpenTextDocumentParams = struct {
    /// The document that was opened.
    textDocument: TextDocumentItem,
};

/// The params sent in a save notebook document notification.
pub const DidSaveNotebookDocumentParams = struct {
    /// The notebook document that got saved.
    notebookDocument: NotebookDocumentIdentifier,
};

pub const DidSaveTextDocumentParams = struct {
    /// Optional the content when saved. Depends on the includeText value when the save notification was requested.
    text: []const u8,
    /// The document that was saved.
    textDocument: TextDocumentIdentifier,
};

pub const DocumentColorClientCapabilities = struct {
    /// * Whether document color supports dynamic registration.
    dynamicRegistration: bool,
};

pub const DocumentColorOptions = struct {
    workDoneProgress: bool,
};

pub const DocumentColorParams = struct {
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// * The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const DocumentColorRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    /// The id used to register the request. The id can be used to deregister the request again. See also Registration#id.
    id: []const u8,
    workDoneProgress: bool,
};

/// Parameters of the document diagnostic request.
pub const DocumentDiagnosticParams = struct {
    /// The additional identifier  provided during registration.
    identifier: []const u8,
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// The result id of a previous response if provided.
    previousResultId: []const u8,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

/// The result of a document diagnostic pull request. A report can either be a full report containing all diagnostics for the requested document or a unchanged report indicating that nothing has changed in terms of diagnostics in comparison to the last pull request.
pub const DocumentDiagnosticReport = union(enum) {
    modify_0: RelatedFullDocumentDiagnosticReport,
    modify_1: RelatedUnchangedDocumentDiagnosticReport,
};

/// The document diagnostic report kinds.
pub const DocumentDiagnosticReportKind = enum(u16) {
    full = 0,
    unchanged = 1,
};

/// A partial result for a document diagnostic report.
pub const DocumentDiagnosticReportPartialResult = struct {
    relatedDocuments: void,
};

pub const DocumentFilter = struct {
    /// A language id, like `typescript`.
    language: []const u8,
    /// A glob pattern, like `*.{ts,js}`.
    ///
    /// Glob patterns can have the following syntax:
    /// - `*` to match one or more characters in a path segment
    /// - `?` to match on one character in a path segment
    /// - `**` to match any number of path segments, including none
    /// - `{}` to group sub patterns into an OR expression. (e.g. `**​/*.{ts,js}`   matches all TypeScript and JavaScript files)
    /// - `[]` to declare a range of characters to match in a path segment   (e.g., `example.[0-9]` to match on `example.0`, `example.1`, …)
    /// - `[!...]` to negate a range of characters to match in a path segment   (e.g., `example.[!0-9]` to match on `example.a`, `example.b`, but   not `example.0`)
    pattern: []const u8,
    /// A Uri [scheme](#Uri.scheme), like `file` or `untitled`.
    scheme: []const u8,
};

pub const DocumentFormattingClientCapabilities = struct {
    /// * Whether formatting supports dynamic registration.
    dynamicRegistration: bool,
};

pub const DocumentFormattingOptions = struct {
    workDoneProgress: bool,
};

pub const DocumentFormattingParams = struct {
    /// * The format options.
    options: FormattingOptions,
    /// * The document to format.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const DocumentFormattingRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    workDoneProgress: bool,
};

/// A document highlight is a range inside a text document which deserves special attention. Usually a document highlight is visualized by changing the background color of its range.
pub const DocumentHighlight = struct {
    /// The highlight kind, default is DocumentHighlightKind.Text.
    kind: DocumentHighlightKind,
    /// The range this highlight applies to.
    range: Range,
};

pub const DocumentHighlightClientCapabilities = struct {
    /// Whether document highlight supports dynamic registration.
    dynamicRegistration: bool,
};

/// A document highlight kind.
pub const DocumentHighlightKind = enum(u16) {
    Text = 1,
    Read = 2,
    Write = 3,
};

pub const DocumentHighlightOptions = struct {
    workDoneProgress: bool,
};

pub const DocumentHighlightParams = struct {
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// The position inside the text document.
    position: Position,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

/// A document link is a range in a text document that links to an internal or external resource, like another text document or a web site.
pub const DocumentLink = struct {
    /// A data entry field that is preserved on a document link between a DocumentLinkRequest and a DocumentLinkResolveRequest.
    data: LSPAny,
    /// The range this link applies to.
    range: Range,
    /// The uri this link points to. If missing a resolve request is sent later.
    target: URI,
    /// The tooltip text when you hover over this link.
    ///
    /// If a tooltip is provided, is will be displayed in a string that includes instructions on how to trigger the link, such as `{0} (ctrl + click)`. The specific instructions vary depending on OS, user settings, and localization.
    tooltip: []const u8,
};

pub const DocumentLinkClientCapabilities = struct {
    /// Whether document link supports dynamic registration.
    dynamicRegistration: bool,
    /// Whether the client supports the `tooltip` property on `DocumentLink`.
    tooltipSupport: bool,
};

pub const DocumentLinkOptions = struct {
    /// Document links have a resolve provider as well.
    resolveProvider: bool,
    workDoneProgress: ?bool = null,
};

pub const DocumentLinkParams = struct {
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// The document to provide document links for.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const DocumentLinkRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    /// Document links have a resolve provider as well.
    resolveProvider: bool,
    workDoneProgress: bool,
};

pub const DocumentOnTypeFormattingClientCapabilities = struct {
    /// * Whether on type formatting supports dynamic registration.
    dynamicRegistration: bool,
};

pub const DocumentOnTypeFormattingOptions = struct {
    /// * A character on which formatting should be triggered, like `{`.
    firstTriggerCharacter: []const u8,
    /// * More trigger characters.
    moreTriggerCharacter: [][]const u8,
};

pub const DocumentOnTypeFormattingParams = struct {
    /// * The character that has been typed that triggered the formatting    * on type request. That is not necessarily the last character that      * got inserted into the document since the client could auto insert        * characters as well (e.g. like automatic brace completion).
    ch: []const u8,
    /// * The formatting options.
    options: FormattingOptions,
    /// * The position around which the on type formatting should happen.   * This is not necessarily the exact position where the character denoted     * by the property `ch` got typed.
    position: Position,
    /// * The document to format.
    textDocument: TextDocumentIdentifier,
};

pub const DocumentOnTypeFormattingRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    /// * A character on which formatting should be triggered, like `{`.
    firstTriggerCharacter: []const u8,
    /// * More trigger characters.
    moreTriggerCharacter: [][]const u8,
};

pub const DocumentRangeFormattingClientCapabilities = struct {
    /// * Whether formatting supports dynamic registration.
    dynamicRegistration: bool,
};

pub const DocumentRangeFormattingOptions = struct {
    workDoneProgress: bool,
};

pub const DocumentRangeFormattingParams = struct {
    /// * The format options
    options: FormattingOptions,
    /// * The range to format
    range: Range,
    /// * The document to format.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const DocumentRangeFormattingRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    workDoneProgress: bool,
};

pub const DocumentSelector = []DocumentFilter;

/// Represents programming constructs like variables, classes, interfaces etc. that appear in a document. Document symbols can be hierarchical and they have two ranges: one that encloses its definition and one that points to its most interesting range, e.g. the range of an identifier.
pub const DocumentSymbol = struct {
    /// Children of this symbol, e.g. properties of a class.
    children: []DocumentSymbol,
    /// Indicates if this symbol is deprecated.
    deprecated: bool,
    /// More detail for this symbol, e.g the signature of a function.
    detail: []const u8,
    /// The kind of this symbol.
    kind: SymbolKind,
    /// The name of this symbol. Will be displayed in the user interface and therefore must not be an empty string or a string only consisting of white spaces.
    name: []const u8,
    /// The range enclosing this symbol not including leading/trailing whitespace but everything else like comments. This information is typically used to determine if the clients cursor is inside the symbol to reveal in the symbol in the UI.
    range: Range,
    /// The range that should be selected and revealed when this symbol is being picked, e.g. the name of a function. Must be contained by the `range`.
    selectionRange: Range,
    /// Tags for this document symbol.
    tags: []SymbolTag,
};

pub const DocumentSymbolClientCapabilities = struct {
    /// Whether document symbol supports dynamic registration.
    dynamicRegistration: bool,
    /// The client supports hierarchical document symbols.
    hierarchicalDocumentSymbolSupport: bool,
    /// The client supports an additional label presented in the UI when registering a document symbol provider.
    labelSupport: bool,
    /// Specific capabilities for the `SymbolKind` in the `textDocument/documentSymbol` request.
    symbolKind: struct {
        /// The symbol kind values the client supports. When this property exists the client also guarantees that it will handle values outside its set gracefully and falls back to a default value when unknown.
        ///
        /// If this property is not present the client only supports the symbol kinds from `File` to `Array` as defined in the initial version of the protocol.
        valueSet: []SymbolKind,
    },
    /// The client supports tags on `SymbolInformation`. Tags are supported on `DocumentSymbol` if `hierarchicalDocumentSymbolSupport` is set to true. Clients supporting tags have to handle unknown tags gracefully.
    tagSupport: struct {
        /// The tags supported by the client.
        valueSet: []SymbolTag,
    },
};

pub const DocumentSymbolOptions = struct {
    /// A human-readable string that is shown when multiple outlines trees are shown for the same document.
    label: []const u8,
    workDoneProgress: bool,
};

pub const DocumentSymbolParams = struct {
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const DocumentSymbolRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    /// A human-readable string that is shown when multiple outlines trees are shown for the same document.
    label: []const u8,
    workDoneProgress: bool,
};

pub const DocumentUri = []const u8;

pub const ExecuteCommandClientCapabilities = struct {
    /// Execute command supports dynamic registration.
    dynamicRegistration: bool,
};

pub const ExecuteCommandOptions = struct {
    /// The commands to be executed on the server
    commands: [][]const u8,
    workDoneProgress: ?bool = null,
};

pub const ExecuteCommandParams = struct {
    /// Arguments that the command should be invoked with.
    arguments: []LSPAny,
    /// The identifier of the actual command handler.
    command: []const u8,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

/// Execute command registration options.
pub const ExecuteCommandRegistrationOptions = struct {
    /// The commands to be executed on the server
    commands: [][]const u8,
    workDoneProgress: bool,
};

pub const ExecutionSummary = struct {
    /// A strict monotonically increasing value indicating the execution order of a cell inside a notebook.
    executionOrder: uinteger,
    /// Whether the execution was successful or not if known by the client.
    success: bool,
};

pub const FailureHandlingKind = enum(u16) {
    /// Applying the workspace change is simply aborted if one of the changes
    /// provided fails. All operations executed before the failing operation
    /// stay executed.
    abort = 0,
    /// All operations are executed transactional. That means they either all
    /// succeed or no changes at all are applied to the workspace.
    transactional = 1,
    /// The client tries to undo the operations already executed. But there is no
    /// guarantee that this is succeeding.
    undo = 2,
    /// If the workspace edit contains only textual file changes they are
    /// executed transactional. If resource changes (create, rename or delete
    /// file) are part of the change the failure handling strategy is abort.
    textOnlyTransactional = 3,

    pub const jsonStringify = enumStringJsonStringify;
};

/// The file event type.
pub const FileChangeType = enum(u16) {
    Created = 1,
    Changed = 2,
    Deleted = 3,
};

/// * Represents information on a file/folder create.  *   * @since 3.16.0
pub const FileCreate = struct {
    /// * A file:// URI for the location of the file/folder being created.
    uri: []const u8,
};

/// * Represents information on a file/folder delete.  *   * @since 3.16.0
pub const FileDelete = struct {
    /// * A file:// URI for the location of the file/folder being deleted.
    uri: []const u8,
};

/// An event describing a file change.
pub const FileEvent = struct {
    /// The change type.
    type: uinteger,
    /// The file's URI.
    uri: DocumentUri,
};

/// * A filter to describe in which file operation requests or notifications  * the server is interested in.   *    * @since 3.16.0
pub const FileOperationFilter = struct {
    /// * The actual file operation pattern.
    pattern: FileOperationPattern,
    /// * A Uri like `file` or `untitled`.
    scheme: []const u8,
};

/// * A pattern to describe in which file operation requests or notifications  * the server is interested in.   *    * @since 3.16.0
pub const FileOperationPattern = struct {
    /// * The glob pattern to match. Glob patterns can have the following syntax:   * - `*` to match one or more characters in a path segment      * - `?` to match on one character in a path segment        * - `**` to match any number of path segments, including none          * - `{}` to group sub patterns into an OR expression. (e.g. `**​/*.{ts,js}`            *   matches all TypeScript and JavaScript files)              * - `[]` to declare a range of characters to match in a path segment                *   (e.g., `example.[0-9]` to match on `example.0`, `example.1`, …)                  * - `[!...]` to negate a range of characters to match in a path segment                    *   (e.g., `example.[!0-9]` to match on `example.a`, `example.b`, but                      *   not `example.0`)
    glob: []const u8,
    /// * Whether to match files or folders with this pattern.    *      * Matches both if undefined.
    matches: FileOperationPatternKind,
    /// * Additional options used during matching.
    options: FileOperationPatternOptions,
};

/// * A pattern kind describing if a glob pattern matches a file a folder or  * both.   *    * @since 3.16.0
pub const FileOperationPatternKind = enum(u16) {
    file = 0,
    folder = 1,

    pub const jsonStringify = enumStringJsonStringify;
};

/// * Matching options for the file operation pattern.  *   * @since 3.16.0
pub const FileOperationPatternOptions = struct {
    /// * The pattern should be matched ignoring casing.
    ignoreCase: bool,
};

/// * The options to register for file operations.  *   * @since 3.16.0
pub const FileOperationRegistrationOptions = struct {
    /// * The actual filters.
    filters: []FileOperationFilter,
};

/// * Represents information on a file/folder rename.  *   * @since 3.16.0
pub const FileRename = struct {
    /// * A file:// URI for the new location of the file/folder being renamed.
    newUri: []const u8,
    /// * A file:// URI for the original location of the file/folder being renamed.
    oldUri: []const u8,
};

pub const FileSystemWatcher = struct {
    /// The glob pattern to watch. See  {@link  GlobPattern glob pattern }
    /// for more detail.
    globPattern: GlobPattern,
    /// The kind of events of interest. If omitted it defaults to WatchKind.Create | WatchKind.Change | WatchKind.Delete which is 7.
    kind: WatchKind,
};

/// Represents a folding range. To be valid, start and end line must be bigger than zero and smaller than the number of lines in the document. Clients are free to ignore invalid ranges.
pub const FoldingRange = struct {
    /// The text that the client should show when the specified range is collapsed. If not defined or not supported by the client, a default will be chosen by the client.
    collapsedText: []const u8,
    /// The zero-based character offset before the folded range ends. If not defined, defaults to the length of the end line.
    endCharacter: uinteger,
    /// The zero-based end line of the range to fold. The folded area ends with the line's last character. To be valid, the end must be zero or larger and smaller than the number of lines in the document.
    endLine: uinteger,
    /// Describes the kind of the folding range such as `comment` or `region`. The kind is used to categorize folding ranges and used by commands like 'Fold all comments'. See [FoldingRangeKind](#FoldingRangeKind) for an enumeration of standardized kinds.
    kind: FoldingRangeKind,
    /// The zero-based character offset from where the folded range starts. If not defined, defaults to the length of the start line.
    startCharacter: uinteger,
    /// The zero-based start line of the range to fold. The folded area starts after the line's last character. To be valid, the end must be zero or larger and smaller than the number of lines in the document.
    startLine: uinteger,
};

pub const FoldingRangeClientCapabilities = struct {
    /// Whether implementation supports dynamic registration for folding range providers. If this is set to `true` the client supports the new `FoldingRangeRegistrationOptions` return value for the corresponding server capability as well.
    dynamicRegistration: bool,
    /// Specific options for the folding range.
    foldingRange: struct {
        /// If set, the client signals that it supports setting collapsedText on folding ranges to display custom labels instead of the default text.
        collapsedText: bool,
    },
    /// Specific options for the folding range kind.
    foldingRangeKind: struct {
        /// The folding range kind values the client supports. When this property exists the client also guarantees that it will handle values outside its set gracefully and falls back to a default value when unknown.
        valueSet: []FoldingRangeKind,
    },
    /// If set, the client signals that it only supports folding complete lines. If set, client will ignore specified `startCharacter` and `endCharacter` properties in a FoldingRange.
    lineFoldingOnly: bool,
    /// The maximum number of folding ranges that the client prefers to receive per document. The value serves as a hint, servers are free to follow the limit.
    rangeLimit: uinteger,
};

/// A set of predefined range kinds.
///  The type is a string since the value set is extensible
pub const FoldingRangeKind = []const u8;

pub const FoldingRangeOptions = struct {
    workDoneProgress: bool,
};

pub const FoldingRangeParams = struct {
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const FoldingRangeRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        selection: DocumentSelector,
    },
    /// The id used to register the request. The id can be used to deregister the request again. See also Registration#id.
    id: []const u8,
    workDoneProgress: bool,
};

/// * Value-object describing what options formatting should use.
pub const FormattingOptions = struct {
    /// * Insert a newline character at the end of the file if one does not exist.   *     * @since 3.15.0
    insertFinalNewline: bool,
    /// * Prefer spaces over tabs.
    insertSpaces: bool,
    /// * Size of a tab in spaces.
    tabSize: uinteger,
    /// * Trim all newlines after the final newline at the end of the file.   *     * @since 3.15.0
    trimFinalNewlines: bool,
    /// * Trim trailing whitespace on a line.   *     * @since 3.15.0
    trimTrailingWhitespace: bool,
};

/// A diagnostic report with a full set of problems.
pub const FullDocumentDiagnosticReport = struct {
    /// The actual items.
    items: []Diagnostic,
    /// A full document diagnostic report.
    kind: DocumentDiagnosticReportKind,
    /// An optional result id. If provided it will be sent on the next diagnostic request for the same document.
    resultId: []const u8,
};

/// The glob pattern. Either a string pattern or a relative pattern.
pub const GlobPattern = union(enum) {
    modify_0: Pattern,
    modify_1: RelativePattern,
};

/// The result of a hover request.
pub const Hover = struct {
    /// The hover's content
    contents: union(enum) {
        modify_0: MarkedString,
        modify_1: []MarkedString,
        modify_2: MarkupContent,
    },
    /// An optional range is a range inside a text document that is used to visualize a hover, e.g. by changing the background color.
    range: Range,
};

pub const HoverClientCapabilities = struct {
    /// Client supports the follow content formats if the content property refers to a `literal of type MarkupContent`. The order describes the preferred format of the client.
    contentFormat: []MarkupKind,
    /// Whether hover supports dynamic registration.
    dynamicRegistration: bool,
};

pub const HoverOptions = struct {
    workDoneProgress: bool,
};

pub const HoverParams = struct {
    /// The position inside the text document.
    position: Position,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const HoverRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    workDoneProgress: bool,
};

pub const HoverResult = struct {
    value: []const u8,
};

pub const ImplementationClientCapabilities = struct {
    /// Whether implementation supports dynamic registration. If this is set to `true` the client supports the new `ImplementationRegistrationOptions` return value for the corresponding server capability as well.
    dynamicRegistration: bool,
    /// The client supports additional metadata in the form of definition links.
    linkSupport: bool,
};

pub const ImplementationOptions = struct {
    workDoneProgress: bool,
};

pub const ImplementationParams = struct {
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// The position inside the text document.
    position: Position,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const ImplementationRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?DocumentSelector,
    /// The id used to register the request. The id can be used to deregister the request again. See also Registration#id.
    id: []const u8,
    workDoneProgress: bool,
};

pub const InitializeError = struct {
    /// Indicates whether the client execute the following retry logic: (1) show the message provided by the ResponseError to the user (2) user selects retry or cancel (3) if user selected retry the initialize method is sent again.
    retry: bool,
};

pub const InitializeParams = struct {
    /// The capabilities provided by the client (editor or tool)
    capabilities: ClientCapabilities = .{},
    /// Information about the client
    clientInfo: ?struct {
        /// The name of the client as defined by the client.
        name: []const u8,
        /// The client's version as defined by the client.
        version: []const u8,
    } = null,
    /// User provided initialization options.
    initializationOptions: LSPAny = .null,
    /// The locale the client is currently showing the user interface in. This must not necessarily be the locale of the operating system.
    ///
    /// Uses IETF language tags as the value's syntax (See https://en.wikipedia.org/wiki/IETF_language_tag)
    locale: []const u8 = "utf-8",
    /// The process Id of the parent process that started the server. Is null if the process has not been started by another process. If the parent process is not alive then the server should exit (see exit notification) its process.
    processId: ?integer = null,
    /// The rootPath of the workspace. Is null if no folder is open.
    rootPath: ?[]const u8 = null,
    /// The rootUri of the workspace. Is null if no folder is open. If both `rootPath` and `rootUri` are set `rootUri` wins.
    rootUri: ?DocumentUri = null,
    /// The initial trace setting. If omitted trace is disabled ('off').
    trace: TraceValue = .off,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ?ProgressToken = null,
    /// The workspace folders configured in the client when the server starts. This property is only available if the client supports workspace folders. It can be `null` if the client supports workspace folders but none are configured.
    workspaceFolders: ?[]WorkspaceFolder = null,
};

pub const InitializeResult = struct {
    /// The capabilities the language server provides.
    capabilities: ServerCapabilities,
    /// Information about the server.
    serverInfo: struct {
        /// The name of the server as defined by the server.
        name: []const u8,
        /// The server's version as defined by the server.
        version: []const u8,
    },
};

pub const InitializedParams = struct {
    pub fn jsonStringify(self: @This(), options: std.json.StringifyOptions, out_stream: anytype) !void {
        _ = options;
        _ = self;
        _ = try out_stream.write("{}");
    }
};

/// Inlay hint information.
pub const InlayHint = struct {
    /// A data entry field that is preserved on an inlay hint between a `textDocument/inlayHint` and a `inlayHint/resolve` request.
    data: LSPAny,
    /// The kind of this hint. Can be omitted in which case the client should fall back to a reasonable default.
    kind: InlayHintKind,
    /// The label of this hint. A human readable string or an array of InlayHintLabelPart label parts.
    ///
    /// *Note* that neither the string nor the label part can be empty.
    label: union(enum) {
        str: []const u8,
        lbl: []InlayHintLabelPart,
    },
    /// Render padding before the hint.
    ///
    /// Note: Padding should use the editor's background color, not the background color of the hint itself. That means padding can be used to visually align/separate an inlay hint.
    paddingLeft: bool,
    /// Render padding after the hint.
    ///
    /// Note: Padding should use the editor's background color, not the background color of the hint itself. That means padding can be used to visually align/separate an inlay hint.
    paddingRight: bool,
    /// The position of this hint.
    position: Position,
    /// Optional text edits that are performed when accepting this inlay hint.
    ///
    /// *Note* that edits are expected to change the document so that the inlay hint (or its nearest variant) is now part of the document and the inlay hint itself is now obsolete.
    ///
    /// Depending on the client capability `inlayHint.resolveSupport` clients might resolve this property late using the resolve request.
    textEdits: []TextEdit,
    /// The tooltip text when you hover over this item.
    ///
    /// Depending on the client capability `inlayHint.resolveSupport` clients might resolve this property late using the resolve request.
    tooltip: union(enum) {
        modify_0: []const u8,
        modify_1: MarkupContent,
    },
};

/// Inlay hint client capabilities.
pub const InlayHintClientCapabilities = struct {
    /// Whether inlay hints support dynamic registration.
    dynamicRegistration: bool,
    /// Indicates which properties a client can resolve lazily on an inlay hint.
    resolveSupport: struct {
        /// The properties that a client can resolve lazily.
        properties: [][]const u8,
    },
};

/// Inlay hint kinds.
pub const InlayHintKind = enum(u16) {
    Type = 1,
    Parameter = 2,
};

/// An inlay hint label part allows for interactive and composite labels of inlay hints.
pub const InlayHintLabelPart = struct {
    /// An optional command for this label part.
    ///
    /// Depending on the client capability `inlayHint.resolveSupport` clients might resolve this property late using the resolve request.
    command: Command,
    /// An optional source code location that represents this label part.
    ///
    /// The editor will use this location for the hover and for code navigation features: This part will become a clickable link that resolves to the definition of the symbol at the given location (not necessarily the location itself), it shows the hover that shows at the given location, and it shows a context menu with further code navigation commands.
    ///
    /// Depending on the client capability `inlayHint.resolveSupport` clients might resolve this property late using the resolve request.
    location: Location,
    /// The tooltip text when you hover over this label part. Depending on the client capability `inlayHint.resolveSupport` clients might resolve this property late using the resolve request.
    tooltip: union(enum) {
        modify_0: []const u8,
        modify_1: MarkupContent,
    },
    /// The value of this label part.
    value: []const u8,
};

/// Inlay hint options used during static registration.
pub const InlayHintOptions = struct {
    /// The server provides support to resolve additional information for an inlay hint item.
    resolveProvider: bool,
    workDoneProgress: bool,
};

/// A parameter literal used in inlay hint requests.
pub const InlayHintParams = struct {
    /// The visible document range for which inlay hints should be computed.
    range: Range,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

/// Inlay hint options used during static or dynamic registration.
pub const InlayHintRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    /// The id used to register the request. The id can be used to deregister the request again. See also Registration#id.
    id: []const u8,
    /// The server provides support to resolve additional information for an inlay hint item.
    resolveProvider: bool,
    workDoneProgress: bool,
};

/// Client workspace capabilities specific to inlay hints.
pub const InlayHintWorkspaceClientCapabilities = struct {
    /// Whether the client implementation supports a refresh request sent from the server to the client.
    ///
    /// Note that this event is global and will force the client to refresh all inlay hints currently shown. It should be used with absolute care and is useful for situation where a server for example detects a project wide change that requires such a calculation.
    refreshSupport: bool,
};

/// Inline value information can be provided by different means:
/// - directly as a text value (class InlineValueText).
/// - as a name to use for a variable lookup (class InlineValueVariableLookup)
/// - as an evaluatable expression (class InlineValueEvaluatableExpression) The InlineValue types combines all inline value types into one type.
pub const InlineValue = union(enum) {
    modify_0: InlineValueText,
    modify_1: InlineValueVariableLookup,
    modify_2: InlineValueEvaluatableExpression,
};

/// Client capabilities specific to inline values.
pub const InlineValueClientCapabilities = struct {
    /// Whether implementation supports dynamic registration for inline value providers.
    dynamicRegistration: bool,
};

pub const InlineValueContext = struct {
    /// The stack frame (as a DAP Id) where the execution has stopped.
    frameId: integer,
    /// The document range where execution has stopped. Typically the end position of the range denotes the line where the inline values are shown.
    stoppedLocation: Range,
};

/// Provide an inline value through an expression evaluation.
///
/// If only a range is specified, the expression will be extracted from the underlying document.
///
/// An optional expression can be used to override the extracted expression.
pub const InlineValueEvaluatableExpression = struct {
    /// If specified the expression overrides the extracted expression.
    expression: []const u8,
    /// The document range for which the inline value applies. The range is used to extract the evaluatable expression from the underlying document.
    range: Range,
};

/// Inline value options used during static registration.
pub const InlineValueOptions = struct {
    workDoneProgress: bool,
};

/// A parameter literal used in inline value requests.
pub const InlineValueParams = struct {
    /// Additional information about the context in which inline values were requested.
    context: InlineValueContext,
    /// The document range for which inline values should be computed.
    range: Range,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

/// Inline value options used during static or dynamic registration.
pub const InlineValueRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    /// The id used to register the request. The id can be used to deregister the request again. See also Registration#id.
    id: []const u8,
    workDoneProgress: bool,
};

/// Provide inline value as text.
pub const InlineValueText = struct {
    /// The document range for which the inline value applies.
    range: Range,
    /// The text of the inline value.
    text: []const u8,
};

/// Provide inline value through a variable lookup.
///
/// If only a range is specified, the variable name will be extracted from the underlying document.
///
/// An optional variable name can be used to override the extracted name.
pub const InlineValueVariableLookup = struct {
    /// How to perform the lookup.
    caseSensitiveLookup: bool,
    /// The document range for which the inline value applies. The range is used to extract the variable name from the underlying document.
    range: Range,
    /// If specified the name of the variable to look up.
    variableName: []const u8,
};

/// Client workspace capabilities specific to inline values.
pub const InlineValueWorkspaceClientCapabilities = struct {
    /// Whether the client implementation supports a refresh request sent from the server to the client.
    ///
    /// Note that this event is global and will force the client to refresh all inline values currently shown. It should be used with absolute care and is useful for situation where a server for example detect a project wide change that requires such a calculation.
    refreshSupport: bool,
};

/// A special text edit to provide an insert and a replace operation.
pub const InsertReplaceEdit = struct {
    /// The range if the insert is requested
    insert: Range,
    /// The string to be inserted.
    newText: []const u8,
    /// The range if the replace is requested.
    replace: Range,
};

/// Defines whether the insert text in a completion item should be interpreted as plain text or a snippet.
pub const InsertTextFormat = enum(u16) {
    PlainText = 1,
    Snippet = 2,
};

/// How whitespace and indentation is handled during completion item insertion.
pub const InsertTextMode = enum(u16) {
    asIs = 1,
    adjustIndentation = 2,

    pub const jsonStringify = enumIntJsonStringify;
};

/// The LSP any type
pub const LSPAny = std.json.Value;

/// LSP arrays.
pub const LSPArray = []LSPAny;

/// LSP object definition.
pub const LSPObject = std.json.ObjectMap;

pub const LinkedEditingRangeClientCapabilities = struct {
    /// * Whether the implementation supports dynamic registration.   * If this is set to `true` the client supports the new     * `(TextDocumentRegistrationOptions & StaticRegistrationOptions)`       * return value for the corresponding server capability as well.
    dynamicRegistration: bool,
};

pub const LinkedEditingRangeOptions = struct {
    workDoneProgress: bool,
};

pub const LinkedEditingRangeParams = struct {
    /// The position inside the text document.
    position: Position,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const LinkedEditingRangeRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    /// The id used to register the request. The id can be used to deregister the request again. See also Registration#id.
    id: []const u8,
    workDoneProgress: bool,
};

pub const LinkedEditingRanges = struct {
    /// * A list of ranges that can be renamed together. The ranges must have   * identical length and contain identical text content. The ranges cannot     * overlap.
    ranges: []Range,
    /// * An optional word pattern (regular expression) that describes valid   * contents for the given ranges. If no pattern is provided, the client     * configuration's word pattern will be used.
    wordPattern: []const u8,
};

pub const Location = struct {
    range: Range,
    uri: DocumentUri,

    pub fn dupe(location: Location, allocator: std.mem.Allocator) !Location {
        var loc = location;
        loc.uri = try allocator.dupe(u8, loc.uri);
        return loc;
    }

    pub fn dupeS(allocator: std.mem.Allocator, locations: []const Location) ![]Location {
        var locs = try allocator.dupe(Location, locations);
        errdefer allocator.free(locs);
        for (locs, 0..) |loc, i| {
            errdefer for (locs[0..i]) |dup_to_free| allocator.free(dup_to_free.uri);
            locs[i] = try loc.dupe(allocator);
        }

        return locs;
    }
};

pub const LocationLink = struct {
    /// Span of the origin of this link.
    ///
    /// Used as the underlined span for mouse interaction. Defaults to the word range at the mouse position.
    originSelectionRange: Range,
    /// The full target range of this link. If the target for example is a symbol then target range is the range enclosing this symbol not including leading/trailing whitespace but everything else like comments. This information is typically used to highlight the range in the editor.
    targetRange: Range,
    /// The range that should be selected and revealed when this link is being followed, e.g the name of a function. Must be contained by the `targetRange`. See also `DocumentSymbol#range`
    targetSelectionRange: Range,
    /// The target resource identifier of this link.
    targetUri: DocumentUri,

    pub fn dupe(link: LocationLink, allocator: std.mem.Allocator) !LocationLink {
        var d = link;
        d.targetUri = try allocator.dupe(u8, d.targetUri);
        return d;
    }

    pub fn dupeS(allocator: std.mem.Allocator, links: []const LocationLink) ![]LocationLink {
        var dups = try allocator.dupe(LocationLink, links);
        errdefer allocator.free(dups);
        for (dups, 0..) |d, i| {
            errdefer for (dups[0..i]) |dup_to_free| allocator.free(dup_to_free.targetUri);
            dups[i] = try d.dupe(allocator);
        }
        return dups;
    }
};

pub const LogMessageParams = struct {
    /// The actual message
    message: []const u8,
    /// The message type. See  {@link  MessageType }
    type: MessageType,
};

pub const LogTraceParams = struct {
    /// The message to be logged.
    message: []const u8,
    /// Additional information that can be computed if the `trace` configuration is set to `'verbose'`
    verbose: []const u8,
};

/// Client capabilities specific to the used markdown parser.
pub const MarkdownClientCapabilities = struct {
    /// A list of HTML tags that the client allows / supports in Markdown.
    allowedTags: [][]const u8,
    /// The name of the parser.
    parser: []const u8,
    /// The version of the parser.
    version: []const u8,
};

/// MarkedString can be used to render human readable text. It is either a markdown string or a code-block that provides a language and a code snippet. The language identifier is semantically equal to the optional language identifier in fenced code blocks in GitHub issues.
///
/// The pair of a language and a value is an equivalent to markdown: ```${language} ${value} ```
///
/// Note that markdown strings will be sanitized - that means html will be escaped.
pub const MarkedString = union(enum) {
    modify_0: []const u8,
    modify_1: struct {
        language: []const u8,
        value: []const u8,
    },
};

/// A `MarkupContent` literal represents a string value which content is interpreted base on its kind flag. Currently the protocol supports `plaintext` and `markdown` as markup kinds.
///
/// If the kind is `markdown` then the value can contain fenced code blocks like in GitHub issues.
///
/// Here is an example how such a string can be constructed using JavaScript / TypeScript: ```typescript let markdown: MarkdownContent = { 	kind: MarkupKind.Markdown, 	value: [ 		'# Header', 		'Some text', 		'```typescript', 		'someCode();', 		'```' 	].join('\n') }; ```
///
/// *Please Note* that clients might sanitize the return markdown. A client could decide to remove HTML from the markdown to avoid script execution.
pub const MarkupContent = struct {
    /// The type of the Markup
    kind: MarkupKind,
    /// The content itself
    value: []const u8,
};

/// Describes the content type that a client supports in various result literals like `Hover`, `ParameterInfo` or `CompletionItem`.
///
/// Please note that `MarkupKinds` must not start with a `$`. This kinds are reserved for internal usage.
pub const MarkupKind = enum(u16) {
    plaintext = 0,
    markdown = 1,

    pub const jsonStringify = enumIntJsonStringify;
};

pub const MessageActionItem = struct {
    /// A short title like 'Retry', 'Open Log' etc.
    title: []const u8,
};

pub const MessageType = enum(u16) {
    Error = 1,
    Warning = 2,
    Info = 3,
    Log = 4,
};

/// Moniker definition to match LSIF 0.5 moniker definition.
pub const Moniker = struct {
    /// The identifier of the moniker. The value is opaque in LSIF however schema owners are allowed to define the structure if they want.
    identifier: []const u8,
    /// The moniker kind if known.
    kind: MonikerKind,
    /// The scheme of the moniker. For example tsc or .Net
    scheme: []const u8,
    /// The scope in which the moniker is unique
    unique: UniquenessLevel,
};

pub const MonikerClientCapabilities = struct {
    /// Whether implementation supports dynamic registration. If this is set to `true` the client supports the new `(TextDocumentRegistrationOptions & StaticRegistrationOptions)` return value for the corresponding server capability as well.
    dynamicRegistration: bool,
};

/// The moniker kind.
pub const MonikerKind = enum(u16) {
    import = 0,
    @"export" = 1,
    local = 2,
};

pub const MonikerOptions = struct {
    workDoneProgress: bool,
};

pub const MonikerParams = struct {
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// The position inside the text document.
    position: Position,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const MonikerRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    workDoneProgress: bool,
};

/// A notebook cell.
///
/// A cell's document URI must be unique across ALL notebook cells and can therefore be used to uniquely identify a notebook cell or the cell's text document.
pub const NotebookCell = struct {
    /// The URI of the cell's text document content.
    document: DocumentUri,
    /// Additional execution summary information if supported by the client.
    executionSummary: ExecutionSummary,
    /// The cell's kind
    kind: NotebookCellKind,
    /// Additional metadata stored with the cell.
    metadata: LSPObject,
};

/// A change describing how to move a `NotebookCell` array from state S to S'.
pub const NotebookCellArrayChange = struct {
    /// The new cells, if any
    cells: []NotebookCell,
    /// The deleted cells
    deleteCount: uinteger,
    /// The start offset of the cell that changed.
    start: uinteger,
};

/// A notebook cell kind.
pub const NotebookCellKind = enum(u16) {
    Markup = 1,
    Code = 2,
};

/// A notebook cell text document filter denotes a cell text document by different properties.
pub const NotebookCellTextDocumentFilter = struct {
    /// A language id like `python`.
    ///
    /// Will be matched against the language id of the notebook cell document. '*' matches every language.
    language: []const u8,
    /// A filter that matches against the notebook containing the notebook cell. If a string value is provided it matches against the notebook type. '*' matches every notebook.
    notebook: union(enum) {
        modify_0: []const u8,
        modify_1: NotebookDocumentFilter,
    },
};

/// A notebook document.
pub const NotebookDocument = struct {
    /// The cells of a notebook.
    cells: []NotebookCell,
    /// Additional metadata stored with the notebook document.
    metadata: LSPObject,
    /// The type of the notebook.
    notebookType: []const u8,
    /// The notebook document's URI.
    uri: URI,
    /// The version number of this document (it will increase after each change, including undo/redo).
    version: integer,
};

/// A change event for a notebook document.
pub const NotebookDocumentChangeEvent = struct {
    /// Changes to cells
    cells: struct {
        /// Changes to notebook cells properties like its kind, execution summary or metadata.
        data: []NotebookCell,
        /// Changes to the cell structure to add or remove cells.
        structure: struct {
            /// The change to the cell array.
            array: NotebookCellArrayChange,
            /// Additional closed cell text documents.
            didClose: []TextDocumentIdentifier,
            /// Additional opened cell text documents.
            didOpen: []TextDocumentItem,
        },
        /// Changes to the text content of notebook cells.
        textContent: []struct {
            changes: []TextDocumentContentChangeEvent,
            document: VersionedTextDocumentIdentifier,
        },
    },
    /// The changed meta data if any.
    metadata: LSPObject,
};

/// Capabilities specific to the notebook document support.
pub const NotebookDocumentClientCapabilities = struct {
    /// Capabilities specific to notebook document synchronization
    synchronization: NotebookDocumentSyncClientCapabilities,
};

/// A notebook document filter denotes a notebook document by different properties.
pub const NotebookDocumentFilter = union(enum) {
    modify_0: struct {
        /// The type of the enclosing notebook.
        notebookType: []const u8,
        /// A glob pattern.
        pattern: []const u8,
        /// A Uri [scheme](#Uri.scheme), like `file` or `untitled`.
        scheme: []const u8,
    },
    modify_1: struct {
        /// The type of the enclosing notebook.
        notebookType: []const u8,
        /// A glob pattern.
        pattern: []const u8,
        /// A Uri [scheme](#Uri.scheme), like `file` or `untitled`.
        scheme: []const u8,
    },
    modify_2: struct {
        /// The type of the enclosing notebook.
        notebookType: []const u8,
        /// A glob pattern.
        pattern: []const u8,
        /// A Uri [scheme](#Uri.scheme), like `file` or `untitled`.
        scheme: []const u8,
    },
};

/// A literal to identify a notebook document in the client.
pub const NotebookDocumentIdentifier = struct {
    /// The notebook document's URI.
    uri: URI,
};

/// Notebook specific client capabilities.
pub const NotebookDocumentSyncClientCapabilities = struct {
    /// Whether implementation supports dynamic registration. If this is set to `true` the client supports the new `(TextDocumentRegistrationOptions & StaticRegistrationOptions)` return value for the corresponding server capability as well.
    dynamicRegistration: bool,
    /// The client supports sending execution summary data per cell.
    executionSummarySupport: bool,
};

/// Options specific to a notebook plus its cells to be synced to the server.
///
/// If a selector provides a notebook document filter but no cell selector all cells of a matching notebook document will be synced.
///
/// If a selector provides no notebook document filter but only a cell selector all notebook documents that contain at least one matching cell will be synced.
pub const NotebookDocumentSyncOptions = struct {
    /// The notebooks to be synced
    notebookSelector: []union(enum) {
        modify_0: struct {
            /// The cells of the matching notebook to be synced.
            cells: []struct {
                language: []const u8,
            },
            /// The notebook to be synced. If a string value is provided it matches against the notebook type. '*' matches every notebook.
            notebook: union(enum) {
                modify_0: []const u8,
                modify_1: NotebookDocumentFilter,
            },
        },
        modify_1: struct {
            /// The cells of the matching notebook to be synced.
            cells: []struct {
                language: []const u8,
            },
            /// The notebook to be synced. If a string value is provided it matches against the notebook type. '*' matches every notebook.
            notebook: union(enum) {
                modify_0: []const u8,
                modify_1: NotebookDocumentFilter,
            },
        },
    },
    /// Whether save notification should be forwarded to the server. Will only be honored if mode === `notebook`.
    save: bool,
};

/// Registration options specific to a notebook.
pub const NotebookDocumentSyncRegistrationOptions = struct {
    /// The id used to register the request. The id can be used to deregister the request again. See also Registration#id.
    id: []const u8,
    /// The notebooks to be synced
    notebookSelector: []union(enum) {
        modify_0: struct {
            /// The cells of the matching notebook to be synced.
            cells: []struct {
                language: []const u8,
            },
            /// The notebook to be synced. If a string value is provided it matches against the notebook type. '*' matches every notebook.
            notebook: union(enum) {
                modify_0: []const u8,
                modify_1: NotebookDocumentFilter,
            },
        },
        modify_1: struct {
            /// The cells of the matching notebook to be synced.
            cells: []struct {
                language: []const u8,
            },
            /// The notebook to be synced. If a string value is provided it matches against the notebook type. '*' matches every notebook.
            notebook: union(enum) {
                modify_0: []const u8,
                modify_1: NotebookDocumentFilter,
            },
        },
    },
    /// Whether save notification should be forwarded to the server. Will only be honored if mode === `notebook`.
    save: bool,
};

pub const OptionalVersionedTextDocumentIdentifier = struct {
    /// The text document's URI.
    uri: DocumentUri,
    /// The version number of this document. If an optional versioned text document identifier is sent from the server to the client and the file is not open in the editor (the server has not received an open notification before) the server can send `null` to indicate that the version is known and the content on disk is the master (as specified with document content ownership).
    ///
    /// The version number of a document will increase after each change, including undo/redo. The number doesn't need to be consecutive.
    version: ?union(enum) {
        modify_0: integer,
    },
};

/// Represents a parameter of a callable-signature. A parameter can have a label and a doc-comment.
pub const ParameterInformation = struct {
    /// The human-readable doc-comment of this parameter. Will be shown in the UI but can be omitted.
    documentation: union(enum) {
        modify_0: []const u8,
        modify_1: MarkupContent,
    },
    /// The label of this parameter information.
    ///
    /// Either a string or an inclusive start and exclusive end offsets within its containing signature label. (see SignatureInformation.label). The offsets are based on a UTF-16 string representation as `Position` and `Range` does.
    ///
    /// *Note*: a label of type string should be a substring of its containing signature label. Its intended use case is to highlight the parameter label part in the `SignatureInformation.label`.
    label: union(enum) {
        modify_0: []const u8,
        modify_1: []uinteger,
    },
};

pub const PartialResultParams = struct {
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
};

/// The glob pattern to watch relative to the base path. Glob patterns can have the following syntax:
/// - `*` to match one or more characters in a path segment
/// - `?` to match on one character in a path segment
/// - `**` to match any number of path segments, including none
/// - `{}` to group conditions (e.g. `**​/*.{ts,js}` matches all TypeScript                        and JavaScript files)
/// - `[]` to declare a range of characters to match in a path segment   (e.g., `example.[0-9]` to match on `example.0`, `example.1`, …)
/// - `[!...]` to negate a range of characters to match in a path segment   (e.g., `example.[!0-9]` to match on `example.a`, `example.b`,   but not `example.0`)
pub const Pattern = []const u8;

pub const Position = struct {
    /// Character offset on a line in a document (zero-based). The meaning of this offset is determined by the negotiated `PositionEncodingKind`.
    ///
    /// If the character value is greater than the line length it defaults back to the line length.
    character: uinteger,
    /// Line position in a document (zero-based).
    line: uinteger,

    pub fn editor(self: @This()) core.Buffer.Point {
        return .{ .row = self.line, .col = self.character };
    }

    pub fn lsp(point: core.Buffer.Point) Position {
        return .{ .line = @intCast(point.row), .character = @intCast(point.col) };
    }
};

/// A type indicating how positions are encoded, specifically what column offsets mean.
pub const PositionEncodingKind = []const u8;

pub const PrepareRenameParams = struct {
    /// The position inside the text document.
    position: Position,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const PrepareSupportDefaultBehavior = []const u8;

/// A previous result id in a workspace pull request.
pub const PreviousResultId = struct {
    /// The URI for which the client knows a result id.
    uri: DocumentUri,
    /// The value of the previous result id.
    value: []const u8,
};

pub const ProgressToken = union(enum) {
    integer: integer,
    string: []const u8,

    pub const jsonStringify = unionJsonStringify;
};

pub const PublishDiagnosticsClientCapabilities = struct {
    /// Client supports a codeDescription property
    codeDescriptionSupport: bool,
    /// Whether code action supports the `data` property which is preserved between a `textDocument/publishDiagnostics` and `textDocument/codeAction` request.
    dataSupport: bool,
    /// Whether the clients accepts diagnostics with related information.
    relatedInformation: bool,
    /// Client supports the tag property to provide meta data about a diagnostic. Clients supporting tags have to handle unknown tags gracefully.
    tagSupport: struct {
        /// The tags supported by the client.
        valueSet: []DiagnosticTag,
    },
    /// Whether the client interprets the version property of the `textDocument/publishDiagnostics` notification's parameter.
    versionSupport: bool,
};

pub const PublishDiagnosticsParams = struct {
    /// An array of diagnostic information items.
    diagnostics: []Diagnostic,
    /// The URI for which diagnostic information is reported.
    uri: DocumentUri,
    /// Optional the version number of the document the diagnostics are published for.
    version: integer,
};

pub const Range = struct {
    /// The range's start position.
    start: Position,
    /// The range's end position.
    end: Position,

    pub fn editor(self: @This()) core.Buffer.PointRange {
        return .{ .start = self.start.editor(), .end = self.end.editor() };
    }

    pub fn lsp(point: core.Buffer.PointRange) Range {
        return .{ .start = Position.lsp(point.start), .end = Position.lsp(point.end) };
    }
};

pub const ReferenceClientCapabilities = struct {
    /// Whether references supports dynamic registration.
    dynamicRegistration: bool,
};

pub const ReferenceContext = struct {
    /// Include the declaration of the current symbol.
    includeDeclaration: bool,
};

pub const ReferenceOptions = struct {
    workDoneProgress: bool,
};

pub const ReferenceParams = struct {
    context: ReferenceContext,
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// The position inside the text document.
    position: Position,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const ReferenceRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    workDoneProgress: bool,
};

/// General parameters to register for a capability.
pub const Registration = struct {
    /// The id used to register the request. The id can be used to deregister the request again.
    id: []const u8,
    /// The method / capability to register for.
    method: []const u8,
    /// Options necessary for the registration.
    registerOptions: LSPAny,
};

pub const RegistrationParams = struct {
    registrations: []Registration,
};

/// Client capabilities specific to regular expressions.
pub const RegularExpressionsClientCapabilities = struct {
    /// The engine's name.
    engine: []const u8,
    /// The engine's version.
    version: []const u8,
};

/// A full diagnostic report with a set of related documents.
pub const RelatedFullDocumentDiagnosticReport = struct {
    /// The actual items.
    items: []Diagnostic,
    /// A full document diagnostic report.
    kind: DocumentDiagnosticReportKind,
    /// Diagnostics of related documents. This information is useful in programming languages where code in a file A can generate diagnostics in a file B which A depends on. An example of such a language is C/C++ where marco definitions in a file a.cpp and result in errors in a header file b.hpp.
    relatedDocuments: void,
    /// An optional result id. If provided it will be sent on the next diagnostic request for the same document.
    resultId: []const u8,
};

/// An unchanged diagnostic report with a set of related documents.
pub const RelatedUnchangedDocumentDiagnosticReport = struct {
    /// A document diagnostic report indicating no changes to the last result. A server can only return `unchanged` if result ids are provided.
    kind: DocumentDiagnosticReportKind,
    /// Diagnostics of related documents. This information is useful in programming languages where code in a file A can generate diagnostics in a file B which A depends on. An example of such a language is C/C++ where marco definitions in a file a.cpp and result in errors in a header file b.hpp.
    relatedDocuments: void,
    /// A result id which will be sent on the next diagnostic request for the same document.
    resultId: []const u8,
};

/// A relative pattern is a helper to construct glob patterns that are matched relatively to a base URI. The common value for a `baseUri` is a workspace folder root, but it can be another absolute URI as well.
pub const RelativePattern = struct {
    /// A workspace folder or a base URI to which this pattern will be matched against relatively.
    baseUri: union(enum) {
        modify_0: WorkspaceFolder,
        modify_1: URI,
    },
    /// The actual glob pattern;
    pattern: Pattern,
};

pub const RenameClientCapabilities = struct {
    /// * Whether rename supports dynamic registration.
    dynamicRegistration: bool,
    /// * Whether the client honors the change annotations in   * text edits and resource operations returned via the     * rename request's workspace edit by for example presenting       * the workspace edit in the user interface and asking         * for confirmation.           *             * @since 3.16.0
    honorsChangeAnnotations: bool,
    /// * Client supports testing for validity of rename operations   * before execution.     *       * @since version 3.12.0
    prepareSupport: bool,
    /// * Client supports the default behavior result    * (`{ defaultBehavior: boolean }`).      *        * The value indicates the default behavior used by the          * client.            *              * @since version 3.16.0
    prepareSupportDefaultBehavior: PrepareSupportDefaultBehavior,
};

/// Rename file operation
pub const RenameFile = struct {
    /// An optional annotation identifier describing the operation.
    annotationId: ChangeAnnotationIdentifier,
    /// A rename
    kind: []const u8,
    /// The new location.
    newUri: DocumentUri,
    /// The old (existing) location.
    oldUri: DocumentUri,
    /// Rename options.
    options: RenameFileOptions,
};

/// Rename file options
pub const RenameFileOptions = struct {
    /// Ignores if target exists.
    ignoreIfExists: bool,
    /// Overwrite target if existing. Overwrite wins over `ignoreIfExists`
    overwrite: bool,
};

/// * The parameters sent in notifications/requests for user-initiated renames  * of files.   *    * @since 3.16.0
pub const RenameFilesParams = struct {
    /// * An array of all files/folders renamed in this operation. When a folder   * is renamed, only the folder will be included, and not its children.
    files: []FileRename,
};

pub const RenameOptions = struct {
    /// * Renames should be checked and tested before being executed.
    prepareProvider: bool,
    workDoneProgress: bool,
};

pub const RenameParams = struct {
    /// * The new name of the symbol. If the given name is not valid the   * request must return a [ResponseError](#ResponseError) with an     * appropriate message set.
    newName: []const u8,
    /// The position inside the text document.
    position: Position,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const RenameRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    /// * Renames should be checked and tested before being executed.
    prepareProvider: bool,
    workDoneProgress: bool,
};

/// The kind of resource operations supported by the client.
pub const ResourceOperationKind = enum(u16) {
    create = 0,
    rename = 1,
    delete = 2,

    pub const jsonStringify = enumStringJsonStringify;
};

pub const SaveOptions = struct {
    /// The client is supposed to include the content on save.
    includeText: bool,
};

pub const SelectionRange = struct {
    /// The parent selection range containing this range. Therefore `parent.range` must contain `this.range`.
    parent: SelectionRange,
    /// The [range](#Range) of this selection range.
    range: Range,
};

pub const SelectionRangeClientCapabilities = struct {
    /// Whether implementation supports dynamic registration for selection range providers. If this is set to `true` the client supports the new `SelectionRangeRegistrationOptions` return value for the corresponding server capability as well.
    dynamicRegistration: bool,
};

pub const SelectionRangeOptions = struct {
    workDoneProgress: bool,
};

pub const SelectionRangeParams = struct {
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// The positions inside the text document.
    positions: []Position,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const SelectionRangeRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    /// The id used to register the request. The id can be used to deregister the request again. See also Registration#id.
    id: []const u8,
    workDoneProgress: bool,
};

pub const SemanticTokenModifiers = enum(u16) {
    declaration = 0,
    definition = 1,
    readonly = 2,
    static = 3,
    deprecated = 4,
    abstract = 5,
    @"async" = 6,
    modification = 7,
    documentation = 8,
    defaultLibrary = 9,
};

pub const SemanticTokenTypes = enum(u16) {
    namespace = 0,
    type = 1,
    class = 2,
    @"enum" = 3,
    interface = 4,
    @"struct" = 5,
    typeParameter = 6,
    parameter = 7,
    variable = 8,
    property = 9,
    enumMember = 10,
    event = 11,
    function = 12,
    method = 13,
    macro = 14,
    keyword = 15,
    modifier = 16,
    comment = 17,
    string = 18,
    number = 19,
    regexp = 20,
    operator = 21,
    decorator = 22,
};

pub const SemanticTokens = struct {
    /// The actual tokens.
    data: []uinteger,
    /// An optional result id. If provided and clients support delta updating the client will include the result id in the next semantic token request. A server can then instead of computing all semantic tokens again simply send a delta.
    resultId: []const u8,
};

pub const SemanticTokensClientCapabilities = struct {
    /// Whether the client uses semantic tokens to augment existing syntax tokens. If set to `true` client side created syntax tokens and semantic tokens are both used for colorization. If set to `false` the client only uses the returned semantic tokens for colorization.
    ///
    /// If the value is `undefined` then the client behavior is not specified.
    augmentsSyntaxTokens: bool,
    /// Whether implementation supports dynamic registration. If this is set to `true` the client supports the new `(TextDocumentRegistrationOptions & StaticRegistrationOptions)` return value for the corresponding server capability as well.
    dynamicRegistration: bool,
    /// The formats the clients supports.
    formats: []TokenFormat,
    /// Whether the client supports tokens that can span multiple lines.
    multilineTokenSupport: bool,
    /// Whether the client supports tokens that can overlap each other.
    overlappingTokenSupport: bool,
    /// Which requests the client supports and might send to the server depending on the server's capability. Please note that clients might not show semantic tokens or degrade some of the user experience if a range or full request is advertised by the client but not provided by the server. If for example the client capability `requests.full` and `request.range` are both set to true but the server only provides a range provider the client might not render a minimap correctly or might even decide to not show any semantic tokens at all.
    requests: struct {
        /// The client will send the `textDocument/semanticTokens/full` request if the server provides a corresponding handler.
        full: union(enum) {
            modify_0: bool,
            modify_1: struct {
                /// The client will send the `textDocument/semanticTokens/full/delta` request if the server provides a corresponding handler.
                delta: bool,
            },
        },
        /// The client will send the `textDocument/semanticTokens/range` request if the server provides a corresponding handler.
        range: union(enum) {
            modify_0: bool,
            modify_1: void,
        },
    },
    /// Whether the client allows the server to actively cancel a semantic token request, e.g. supports returning ErrorCodes.ServerCancelled. If a server does the client needs to retrigger the request.
    serverCancelSupport: bool,
    /// The token modifiers that the client supports.
    tokenModifiers: [][]const u8,
    /// The token types that the client supports.
    tokenTypes: [][]const u8,
};

pub const SemanticTokensDelta = struct {
    /// The semantic token edits to transform a previous result into a new result.
    edits: []SemanticTokensEdit,
    resultId: []const u8,
};

pub const SemanticTokensDeltaParams = struct {
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// The result id of a previous response. The result Id can either point to a full response or a delta response depending on what was received last.
    previousResultId: []const u8,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const SemanticTokensDeltaPartialResult = struct {
    edits: []SemanticTokensEdit,
};

pub const SemanticTokensEdit = struct {
    /// The elements to insert.
    data: []uinteger,
    /// The count of elements to remove.
    deleteCount: uinteger,
    /// The start offset of the edit.
    start: uinteger,
};

pub const SemanticTokensLegend = struct {
    /// The token modifiers a server uses.
    tokenModifiers: [][]const u8,
    /// The token types a server uses.
    tokenTypes: [][]const u8,
};

pub const SemanticTokensOptions = struct {
    /// Server supports providing semantic tokens for a full document.
    full: union(enum) {
        bool: bool,
        obj: struct {
            /// The server supports deltas for full documents.
            delta: bool,
        },
    },
    /// The legend used by the server
    legend: SemanticTokensLegend,
    /// Server supports providing semantic tokens for a specific range of a document.
    range: union(enum) {
        bool: bool,
        void: void,
    },
    workDoneProgress: ?bool = null,
};

pub const SemanticTokensParams = struct {
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const SemanticTokensPartialResult = struct {
    data: []uinteger,
};

pub const SemanticTokensRangeParams = struct {
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// The range the semantic tokens are requested for.
    range: Range,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const SemanticTokensRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        selector: DocumentSelector,
    },
    /// Server supports providing semantic tokens for a full document.
    full: union(enum) {
        bool: bool,
        obj: struct {
            /// The server supports deltas for full documents.
            delta: bool,
        },
    },
    /// The id used to register the request. The id can be used to deregister the request again. See also Registration#id.
    id: []const u8,
    /// The legend used by the server
    legend: SemanticTokensLegend,
    /// Server supports providing semantic tokens for a specific range of a document.
    range: union(enum) {
        bool: bool,
        void: void,
    },
    workDoneProgress: bool,
};

pub const SemanticTokensWorkspaceClientCapabilities = struct {
    /// Whether the client implementation supports a refresh request sent from the server to the client.
    ///
    /// Note that this event is global and will force the client to refresh all semantic tokens currently shown. It should be used with absolute care and is useful for situation where a server for example detect a project wide change that requires such a calculation.
    refreshSupport: bool,
};

pub const ServerCapabilities = struct {
    /// The server provides call hierarchy support.
    callHierarchyProvider: ?union(enum) {
        bool: bool,
        opts: CallHierarchyOptions,
        regs: CallHierarchyRegistrationOptions,
    } = null,
    /// The server provides code actions. The `CodeActionOptions` return type is only valid if the client signals code action literal support via the property `textDocument.codeAction.codeActionLiteralSupport`.
    codeActionProvider: ?union(enum) { bool: bool, opts: CodeActionOptions } = null,
    /// The server provides code lens.
    codeLensProvider: ?CodeLensOptions = null,
    /// The server provides color provider support.
    colorProvider: ?union(enum) {
        bool: bool,
        opts: DocumentColorOptions,
        regs: DocumentColorRegistrationOptions,
    } = null,
    /// The server provides completion support.
    completionProvider: ?CompletionOptions = null,
    /// The server provides go to declaration support.
    declarationProvider: ?union(enum) {
        bool: bool,
        opts: DeclarationOptions,
        regs: DeclarationRegistrationOptions,
    } = null,
    /// The server provides goto definition support.
    definitionProvider: ?union(enum) { bool: bool, opts: DefinitionOptions } = null,
    /// The server has support for pull model diagnostics.
    diagnosticProvider: ?union(enum) {
        opts: DiagnosticOptions,
        regs: DiagnosticRegistrationOptions,
    } = null,
    /// The server provides document formatting.
    documentFormattingProvider: ?union(enum) {
        bool: bool,
        opts: DocumentFormattingOptions,
    } = null,
    /// The server provides document highlight support.
    documentHighlightProvider: ?union(enum) {
        bool: bool,
        opts: DocumentHighlightOptions,
    } = null,
    /// The server provides document link support.
    documentLinkProvider: ?DocumentLinkOptions = null,
    /// The server provides document formatting on typing.
    documentOnTypeFormattingProvider: ?DocumentOnTypeFormattingOptions = null,

    /// The server provides document range formatting.
    documentRangeFormattingProvider: ?union(enum) {
        bool: bool,
        opts: DocumentRangeFormattingOptions,
    } = null,
    /// The server provides document symbol support.
    documentSymbolProvider: ?union(enum) {
        bool: bool,
        opts: DocumentSymbolOptions,
    } = null,
    /// The server provides execute command support.
    executeCommandProvider: ?ExecuteCommandOptions = null,
    /// Experimental server capabilities.
    experimental: ?LSPAny = null,
    /// The server provides folding provider support.
    foldingRangeProvider: ?union(enum) {
        bool: bool,
        opts: FoldingRangeOptions,
        regs: FoldingRangeRegistrationOptions,
    } = null,
    /// The server provides hover support.
    hoverProvider: ?union(enum) {
        bool: bool,
        opts: HoverOptions,
    } = null,
    /// The server provides goto implementation support.
    implementationProvider: ?union(enum) {
        bool: bool,
        opts: ImplementationOptions,
        regs: ImplementationRegistrationOptions,
    } = null,
    /// The server provides inlay hints.
    inlayHintProvider: ?union(enum) {
        bool: bool,
        opts: InlayHintOptions,
        regs: InlayHintRegistrationOptions,
    } = null,
    /// The server provides inline values.
    inlineValueProvider: ?union(enum) {
        bool: bool,
        opts: InlineValueOptions,
        regs: InlineValueRegistrationOptions,
    } = null,
    /// The server provides linked editing range support.
    linkedEditingRangeProvider: ?union(enum) {
        bool: bool,
        opts: LinkedEditingRangeOptions,
        regs: LinkedEditingRangeRegistrationOptions,
    } = null,

    /// Whether server provides moniker support.
    monikerProvider: ?union(enum) {
        bool: bool,
        opts: MonikerOptions,
        regs: MonikerRegistrationOptions,
    } = null,
    /// Defines how notebook documents are synced.
    notebookDocumentSync: ?union(enum) {
        opts: NotebookDocumentSyncOptions,
        regs: NotebookDocumentSyncRegistrationOptions,
    } = null,
    /// The position encoding the server picked from the encodings offered by the client via the client capability `general.positionEncodings`.
    ///
    /// If the client didn't provide any position encodings the only valid value that a server can return is 'utf-16'.
    ///
    /// If omitted it defaults to 'utf-16'.
    positionEncoding: ?PositionEncodingKind = null,
    /// The server provides find references support.
    referencesProvider: ?union(enum) {
        boo: bool,
        opts: ReferenceOptions,
    } = null,
    /// The server provides rename support. RenameOptions may only be specified if the client states that it supports `prepareSupport` in its initial `initialize` request.
    renameProvider: ?union(enum) {
        bool: bool,
        opts: RenameOptions,
    } = null,
    /// The server provides selection range support.
    selectionRangeProvider: ?union(enum) {
        bool: bool,
        opts: SelectionRangeOptions,
        regs: SelectionRangeRegistrationOptions,
    } = null,
    /// The server provides semantic tokens support.
    semanticTokensProvider: ?union(enum) {
        opts: SemanticTokensOptions,
        regs: SemanticTokensRegistrationOptions,
    } = null,
    /// The server provides signature help support.
    signatureHelpProvider: SignatureHelpOptions,
    /// Defines how text documents are synced. Is either a detailed structure defining each notification or for backwards compatibility the TextDocumentSyncKind number. If omitted it defaults to `TextDocumentSyncKind.None`.
    textDocumentSync: ?union(enum) {
        opts: TextDocumentSyncOptions,
        kind: TextDocumentSyncKind,
    } = null,
    /// The server provides goto type definition support.
    typeDefinitionProvider: ?union(enum) {
        bool: bool,
        opts: TypeDefinitionOptions,
        regs: TypeDefinitionRegistrationOptions,
    } = null,
    /// The server provides type hierarchy support.
    typeHierarchyProvider: ?union(enum) {
        bool: bool,
        opts: TypeHierarchyOptions,
        regs: TypeHierarchyRegistrationOptions,
    } = null,
    /// Workspace specific server capabilities
    workspace: ?struct {
        /// The server is interested in file notifications/requests.
        fileOperations: struct {
            /// The server is interested in receiving didCreateFiles notifications.
            didCreate: ?FileOperationRegistrationOptions = null,
            /// The server is interested in receiving didDeleteFiles file notifications.
            didDelete: ?FileOperationRegistrationOptions = null,
            /// The server is interested in receiving didRenameFiles notifications.
            didRename: ?FileOperationRegistrationOptions = null,
            /// The server is interested in receiving willCreateFiles requests.
            willCreate: ?FileOperationRegistrationOptions = null,
            /// The server is interested in receiving willDeleteFiles file requests.
            willDelete: ?FileOperationRegistrationOptions = null,
            /// The server is interested in receiving willRenameFiles requests.
            willRename: ?FileOperationRegistrationOptions = null,
        } = .{},
        /// The server supports workspace folder.
        workspaceFolders: ?WorkspaceFoldersServerCapabilities = null,
    } = .{},
    /// The server provides workspace symbol support.
    workspaceSymbolProvider: ?union(enum) {
        bool: bool,
        opts: WorkspaceSymbolOptions,
    } = null,
};

pub const SetTraceParams = struct {
    /// The new value that should be assigned to the trace setting.
    value: TraceValue,
};

/// Client capabilities for the show document request.
pub const ShowDocumentClientCapabilities = struct {
    /// The client has support for the show document request.
    support: bool,
};

/// Params to show a resource.
pub const ShowDocumentParams = struct {
    /// Indicates to show the resource in an external program. To show, for example, `https://code.visualstudio.com/` in the default WEB browser set `external` to `true`.
    external: bool,
    /// An optional selection range if the document is a text document. Clients might ignore the property if an external program is started or the file is not a text file.
    selection: Range,
    /// An optional property to indicate whether the editor showing the document should take focus or not. Clients might ignore this property if an external program is started.
    takeFocus: bool,
    /// The uri to show.
    uri: URI,
};

/// The result of an show document request.
pub const ShowDocumentResult = struct {
    /// A boolean indicating if the show was successful.
    success: bool,
};

pub const ShowMessageParams = struct {
    /// The actual message.
    message: []const u8,
    /// The message type. See  {@link  MessageType } .
    type: MessageType,
};

/// Show message request client capabilities
pub const ShowMessageRequestClientCapabilities = struct {
    /// Capabilities specific to the `MessageActionItem` type.
    messageActionItem: struct {
        /// Whether the client supports additional attributes which are preserved and sent back to the server in the request's response.
        additionalPropertiesSupport: bool,
    },
};

pub const ShowMessageRequestParams = struct {
    /// The message action items to present.
    actions: []MessageActionItem,
    /// The actual message
    message: []const u8,
    /// The message type. See  {@link  MessageType }
    type: MessageType,
};

/// * Signature help represents the signature of something  * callable. There can be multiple signature but only one   * active and only one active parameter.
pub const SignatureHelp = struct {
    /// * The active parameter of the active signature. If omitted or the value    * lies outside the range of `signatures[activeSignature].parameters`      * defaults to 0 if the active signature has parameters. If        * the active signature has no parameters it is ignored.          * In future version of the protocol this property might become            * mandatory to better express the active parameter if the              * active signature does have any.
    activeParameter: uinteger,
    /// * The active signature. If omitted or the value lies outside the   * range of `signatures` the value defaults to zero or is ignore if      * the `SignatureHelp` as no signatures.        *          * Whenever possible implementors should make an active decision about            * the active signature and shouldn't rely on a default value.              *                * In future version of the protocol this property might become                  * mandatory to better express this.
    activeSignature: uinteger,
    /// * One or more signatures. If no signatures are available the signature help   * request should return `null`.
    signatures: []SignatureInformation,
};

pub const SignatureHelpClientCapabilities = struct {
    /// * The client supports to send additional context information for a   * `textDocument/signatureHelp` request. A client that opts into     * contextSupport will also support the `retriggerCharacters` on       * `SignatureHelpOptions`.         *           * @since 3.15.0
    contextSupport: bool,
    /// * Whether signature help supports dynamic registration.
    dynamicRegistration: bool,
    /// * The client supports the following `SignatureInformation`   * specific properties.
    signatureInformation: struct {
        /// * The client supports the `activeParameter` property on     * `SignatureInformation` literal.         *             * @since 3.16.0
        activeParameterSupport: bool,
        /// * Client supports the follow content formats for the documentation     * property. The order describes the preferred format of the client.
        documentationFormat: []MarkupKind,
        /// * Client capabilities specific to parameter information.
        parameterInformation: struct {
            /// * The client supports processing label offsets instead of a       * simple label string.             *                   * @since 3.14.0
            labelOffsetSupport: bool,
        },
    },
};

/// * Additional information about the context in which a signature help request  * was triggered.   *    * @since 3.15.0
pub const SignatureHelpContext = struct {
    /// * The currently active `SignatureHelp`.   *     * The `activeSignatureHelp` has its `SignatureHelp.activeSignature` field       * updated based on the user navigating through available signatures.
    activeSignatureHelp: SignatureHelp,
    /// * `true` if signature help was already showing when it was triggered.   *     * Retriggers occur when the signature help is already active and can be       * caused by actions such as typing a trigger character, a cursor move, or         * document content changes.
    isRetrigger: bool,
    /// * Character that caused signature help to be triggered.   *      * This is undefined when triggerKind !==        * SignatureHelpTriggerKind.TriggerCharacter
    triggerCharacter: []const u8,
    /// * Action that caused signature help to be triggered.
    triggerKind: SignatureHelpTriggerKind,
};

pub const SignatureHelpOptions = struct {
    /// * List of characters that re-trigger signature help.   *      * These trigger characters are only active when signature help is already        * showing. All trigger characters are also counted as re-trigger          * characters.            *              * @since 3.15.0
    retriggerCharacters: [][]const u8 = &.{},
    /// * The characters that trigger signature help   * automatically.
    triggerCharacters: [][]const u8 = &.{},
    workDoneProgress: ?bool = null,
};

pub const SignatureHelpParams = struct {
    /// * The signature help context. This is only available if the client    * specifies to send this using the client capability      * `textDocument.signatureHelp.contextSupport === true`        *          * @since 3.15.0
    context: SignatureHelpContext,
    /// The position inside the text document.
    position: Position,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const SignatureHelpRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    /// * List of characters that re-trigger signature help.   *      * These trigger characters are only active when signature help is already        * showing. All trigger characters are also counted as re-trigger          * characters.            *              * @since 3.15.0
    retriggerCharacters: [][]const u8,
    /// * The characters that trigger signature help   * automatically.
    triggerCharacters: [][]const u8,
    workDoneProgress: bool,
};

/// * How a signature help was triggered.  *   * @since 3.15.0
pub const SignatureHelpTriggerKind = enum(u16) {
    Invoked = 1,
    TriggerCharacter = 2,
    ContentChange = 3,
};

/// * Represents the signature of something callable. A signature  * can have a label, like a function-name, a doc-comment, and   * a set of parameters.
pub const SignatureInformation = struct {
    /// * The index of the active parameter.   *     * If provided, this is used in place of `SignatureHelp.activeParameter`.        *          * @since 3.16.0
    activeParameter: uinteger,
    /// * The human-readable doc-comment of this signature. Will be shown   * in the UI but can be omitted.
    documentation: union(enum) {
        modify_0: []const u8,
        modify_1: MarkupContent,
    },
    /// * The label of this signature. Will be shown in   * the UI.
    label: []const u8,
    /// * The parameters of this signature.
    parameters: []ParameterInformation,
};

/// Static registration options to be returned in the initialize request.
pub const StaticRegistrationOptions = struct {
    /// The id used to register the request. The id can be used to deregister the request again. See also Registration#id.
    id: []const u8,
};

/// Represents information about programming constructs like variables, classes, interfaces etc.
pub const SymbolInformation = struct {
    /// The name of the symbol containing this symbol. This information is for user interface purposes (e.g. to render a qualifier in the user interface if necessary). It can't be used to re-infer a hierarchy for the document symbols.
    containerName: []const u8,
    /// Indicates if this symbol is deprecated.
    deprecated: bool,
    /// The kind of this symbol.
    kind: SymbolKind,
    /// The location of this symbol. The location's range is used by a tool to reveal the location in the editor. If the symbol is selected in the tool the range's start information is used to position the cursor. So the range usually spans more then the actual symbol's name and does normally include things like visibility modifiers.
    ///
    /// The range doesn't have to denote a node range in the sense of an abstract syntax tree. It can therefore not be used to re-construct a hierarchy of the symbols.
    location: Location,
    /// The name of this symbol.
    name: []const u8,
    /// Tags for this symbol.
    tags: []SymbolTag,
};

/// A symbol kind.
pub const SymbolKind = enum(u16) {
    File = 1,
    Module = 2,
    Namespace = 3,
    Package = 4,
    Class = 5,
    Method = 6,
    Property = 7,
    Field = 8,
    Constructor = 9,
    Enum = 10,
    Interface = 11,
    Function = 12,
    Variable = 13,
    Constant = 14,
    String = 15,
    Number = 16,
    Boolean = 17,
    Array = 18,
    Object = 19,
    Key = 20,
    Null = 21,
    EnumMember = 22,
    Struct = 23,
    Event = 24,
    Operator = 25,
    TypeParameter = 26,

    pub const jsonStringify = enumIntJsonStringify;
};

/// Symbol tags are extra annotations that tweak the rendering of a symbol.
pub const SymbolTag = []const u8;

/// Describe options to be used when registering for text document change events.
pub const TextDocumentChangeRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?DocumentSelector = null,
    /// How documents are synced to the server. See TextDocumentSyncKind.Full and TextDocumentSyncKind.Incremental.
    syncKind: TextDocumentSyncKind = .Incremental,
};

/// Text document specific client capabilities.
pub const TextDocumentClientCapabilities = struct {
    /// Capabilities specific to the various call hierarchy requests.
    callHierarchy: CallHierarchyClientCapabilities = .{},
    /// Capabilities specific to the `textDocument/codeAction` request.
    codeAction: ?CodeActionClientCapabilities = null,
    /// Capabilities specific to the `textDocument/codeLens` request.
    codeLens: ?CodeLensClientCapabilities = null,
    /// Capabilities specific to the `textDocument/documentColor` and the `textDocument/colorPresentation` request.
    colorProvider: ?DocumentColorClientCapabilities = null,
    /// Capabilities specific to the `textDocument/completion` request.
    completion: ?CompletionClientCapabilities = null,
    /// Capabilities specific to the `textDocument/declaration` request.
    declaration: ?DeclarationClientCapabilities = .{},
    /// Capabilities specific to the `textDocument/definition` request.
    definition: ?DefinitionClientCapabilities = null,
    /// Capabilities specific to the diagnostic pull model.
    diagnostic: ?DiagnosticClientCapabilities = null,
    /// Capabilities specific to the `textDocument/documentHighlight` request.
    documentHighlight: ?DocumentHighlightClientCapabilities = null,
    /// Capabilities specific to the `textDocument/documentLink` request.
    documentLink: ?DocumentLinkClientCapabilities = null,
    /// Capabilities specific to the `textDocument/documentSymbol` request.
    documentSymbol: ?DocumentSymbolClientCapabilities = null,
    /// Capabilities specific to the `textDocument/foldingRange` request.
    foldingRange: ?FoldingRangeClientCapabilities = null,
    /// Capabilities specific to the `textDocument/formatting` request.
    formatting: ?DocumentFormattingClientCapabilities = null,
    /// Capabilities specific to the `textDocument/hover` request.
    hover: ?HoverClientCapabilities = null,
    /// Capabilities specific to the `textDocument/implementation` request.
    implementation: ?ImplementationClientCapabilities = null,
    /// Capabilities specific to the `textDocument/inlayHint` request.
    inlayHint: ?InlayHintClientCapabilities = null,
    /// Capabilities specific to the `textDocument/inlineValue` request.
    inlineValue: ?InlineValueClientCapabilities = null,
    /// Capabilities specific to the `textDocument/linkedEditingRange` request.
    linkedEditingRange: ?LinkedEditingRangeClientCapabilities = null,
    /// Capabilities specific to the `textDocument/moniker` request.
    moniker: ?MonikerClientCapabilities = null,
    /// request. Capabilities specific to the `textDocument/onTypeFormatting` request.
    onTypeFormatting: ?DocumentOnTypeFormattingClientCapabilities = null,
    /// Capabilities specific to the `textDocument/publishDiagnostics` notification.
    publishDiagnostics: ?PublishDiagnosticsClientCapabilities = null,
    /// Capabilities specific to the `textDocument/rangeFormatting` request.
    rangeFormatting: ?DocumentRangeFormattingClientCapabilities = null,
    /// Capabilities specific to the `textDocument/references` request.
    references: ?ReferenceClientCapabilities = null,
    /// Capabilities specific to the `textDocument/rename` request.
    rename: ?RenameClientCapabilities = null,
    /// Capabilities specific to the `textDocument/selectionRange` request.
    selectionRange: ?SelectionRangeClientCapabilities = null,
    /// Capabilities specific to the various semantic token requests.
    semanticTokens: ?SemanticTokensClientCapabilities = null,
    /// Capabilities specific to the `textDocument/signatureHelp` request.
    signatureHelp: ?SignatureHelpClientCapabilities = null,
    synchronization: ?TextDocumentSyncClientCapabilities = null,
    /// Capabilities specific to the `textDocument/typeDefinition` request.
    typeDefinition: ?TypeDefinitionClientCapabilities = null,
    /// Capabilities specific to the various type hierarchy requests.
    typeHierarchy: ?TypeHierarchyClientCapabilities = null,
};

/// An event describing a change to a text document. If only a text is provided it is considered to be the full content of the document.
pub const TextDocumentContentChangeEvent = union(enum) {
    change: struct {
        /// The range of the document that changed.
        range: Range,
        // The optional length of the range that got replaced.
        // deprecated use range instead.
        // rangeLength: uinteger,
        /// The new text for the provided range.
        text: []const u8,
    },
    all: struct {
        /// The new text of the whole document.
        text: []const u8,
    },

    pub const jsonStringify = unionJsonStringify;
};

pub const TextDocumentEdit = struct {
    /// The edits to be applied.
    edits: []union(enum) {
        modify_0: TextEdit,
        modify_1: AnnotatedTextEdit,
    },
    /// The text document to change.
    textDocument: OptionalVersionedTextDocumentIdentifier,
};

pub const TextDocumentIdentifier = struct {
    /// The text document's URI.
    uri: DocumentUri,
};

pub const TextDocumentItem = struct {
    /// The text document's language identifier.
    languageId: []const u8,
    /// The content of the opened text document.
    text: []const u8,
    /// The text document's URI.
    uri: DocumentUri,
    /// The version number of this document (it will increase after each change, including undo/redo).
    version: integer,
};

pub const TextDocumentPositionParams = struct {
    /// The position inside the text document.
    position: Position,
    /// The text document.
    textDocument: TextDocumentIdentifier,
};

/// General text document registration options.
pub const TextDocumentRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
};

/// Represents reasons why a text document is saved.
pub const TextDocumentSaveReason = enum(u16) {
    Manual = 1,
    AfterDelay = 2,
    FocusOut = 3,
};

pub const TextDocumentSaveRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    /// The client is supposed to include the content on save.
    includeText: bool,
};

pub const TextDocumentSyncClientCapabilities = struct {
    /// The client supports did save notifications.
    didSave: bool,
    /// Whether text document synchronization supports dynamic registration.
    dynamicRegistration: bool,
    /// The client supports sending will save notifications.
    willSave: bool,
    /// The client supports sending a will save request and waits for a response providing text edits which will be applied to the document before it is saved.
    willSaveWaitUntil: bool,
};

/// Defines how the host (editor) should sync document changes to the language server.
pub const TextDocumentSyncKind = enum(u16) {
    None = 0,
    Full = 1,
    Incremental = 2,

    pub const jsonStringify = enumStringJsonStringify;
};

pub const TextDocumentSyncOptions = struct {
    /// Change notifications are sent to the server. See TextDocumentSyncKind.None, TextDocumentSyncKind.Full and TextDocumentSyncKind.Incremental. If omitted it defaults to TextDocumentSyncKind.None.
    change: TextDocumentSyncKind,
    /// Open and close notifications are sent to the server. If omitted open close notifications should not be sent.
    ///  Open and close notifications are sent to the server. If omitted open close notification should not be sent.
    openClose: bool,
};

pub const TextEdit = struct {
    /// The string to be inserted. For delete operations use an empty string.
    newText: []const u8,
    /// The range of the text document to be manipulated. To insert text into a document create a range where start === end.
    range: Range,
};

pub const TokenFormat = []const u8;

pub const TraceValue = enum(u16) {
    off = 0,
    messages = 1,
    verbose = 2,

    pub const jsonStringify = enumStringJsonStringify;
};

pub const TypeDefinitionClientCapabilities = struct {
    /// Whether implementation supports dynamic registration. If this is set to `true` the client supports the new `TypeDefinitionRegistrationOptions` return value for the corresponding server capability as well.
    dynamicRegistration: bool,
    /// The client supports additional metadata in the form of definition links.
    linkSupport: bool,
};

pub const TypeDefinitionOptions = struct {
    workDoneProgress: bool,
};

pub const TypeDefinitionParams = struct {
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// The position inside the text document.
    position: Position,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const TypeDefinitionRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    /// The id used to register the request. The id can be used to deregister the request again. See also Registration#id.
    id: []const u8,
    workDoneProgress: bool,
};

pub const TypeHierarchyClientCapabilities = struct {
    /// Whether implementation supports dynamic registration. If this is set to `true` the client supports the new `(TextDocumentRegistrationOptions & StaticRegistrationOptions)` return value for the corresponding server capability as well.
    dynamicRegistration: bool,
};

pub const TypeHierarchyItem = struct {
    /// A data entry field that is preserved between a type hierarchy prepare and supertypes or subtypes requests. It could also be used to identify the type hierarchy in the server, helping improve the performance on resolving supertypes and subtypes.
    data: LSPAny,
    /// More detail for this item, e.g. the signature of a function.
    detail: []const u8,
    /// The kind of this item.
    kind: SymbolKind,
    /// The name of this item.
    name: []const u8,
    /// The range enclosing this symbol not including leading/trailing whitespace but everything else, e.g. comments and code.
    range: Range,
    /// The range that should be selected and revealed when this symbol is being picked, e.g. the name of a function. Must be contained by the [`range`](#TypeHierarchyItem.range).
    selectionRange: Range,
    /// Tags for this item.
    tags: []SymbolTag,
    /// The resource identifier of this item.
    uri: DocumentUri,
};

pub const TypeHierarchyOptions = struct {
    workDoneProgress: bool,
};

pub const TypeHierarchyPrepareParams = struct {
    /// The position inside the text document.
    position: Position,
    /// The text document.
    textDocument: TextDocumentIdentifier,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const TypeHierarchyRegistrationOptions = struct {
    /// A document selector to identify the scope of the registration. If set to null the document selector provided on the client side will be used.
    documentSelector: ?union(enum) {
        modify_0: DocumentSelector,
    },
    /// The id used to register the request. The id can be used to deregister the request again. See also Registration#id.
    id: []const u8,
    workDoneProgress: bool,
};

pub const TypeHierarchySubtypesParams = struct {
    item: TypeHierarchyItem,
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const TypeHierarchySupertypesParams = struct {
    item: TypeHierarchyItem,
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const URI = []const u8;

/// A diagnostic report indicating that the last returned report is still accurate.
pub const UnchangedDocumentDiagnosticReport = struct {
    /// A document diagnostic report indicating no changes to the last result. A server can only return `unchanged` if result ids are provided.
    kind: DocumentDiagnosticReportKind,
    /// A result id which will be sent on the next diagnostic request for the same document.
    resultId: []const u8,
};

/// Moniker uniqueness level to define scope of the moniker.
pub const UniquenessLevel = enum(u16) {
    document = 0,
    project = 1,
    group = 2,
    scheme = 3,
    global = 4,
};

/// General parameters to unregister a capability.
pub const Unregistration = struct {
    /// The id used to unregister the request or notification. Usually an id provided during the register request.
    id: []const u8,
    /// The method / capability to unregister for.
    method: []const u8,
};

pub const UnregistrationParams = struct {
    unregisterations: []Unregistration,
};

/// A versioned notebook document identifier.
pub const VersionedNotebookDocumentIdentifier = struct {
    /// The notebook document's URI.
    uri: URI,
    /// The version number of this notebook document.
    version: integer,
};

pub const VersionedTextDocumentIdentifier = struct {
    /// The text document's URI.
    uri: DocumentUri,
    /// The version number of this document.
    ///
    /// The version number of a document will increase after each change, including undo/redo. The number doesn't need to be consecutive.
    version: integer,
};

pub const WatchKind = enum(u16) {
    Create = 1,
    Change = 2,
    Delete = 4,
};

/// The parameters send in a will save text document notification.
pub const WillSaveTextDocumentParams = struct {
    /// The 'TextDocumentSaveReason'.
    reason: TextDocumentSaveReason,
    /// The document that will be saved.
    textDocument: TextDocumentIdentifier,
};

pub const WorkDoneProgressBegin = struct {
    /// Controls if a cancel button should show to allow the user to cancel the long running operation. Clients that don't support cancellation are allowed to ignore the setting.
    cancellable: bool,
    kind: []const u8,
    /// Optional, more detailed associated progress message. Contains complementary information to the `title`.
    ///
    /// Examples: "3/25 files", "project/src/module2", "node_modules/some_dep". If unset, the previous progress message (if any) is still valid.
    message: []const u8,
    /// Optional progress percentage to display (value 100 is considered 100%). If not provided infinite progress is assumed and clients are allowed to ignore the `percentage` value in subsequent in report notifications.
    ///
    /// The value should be steadily rising. Clients are free to ignore values that are not following this rule. The value range is [0, 100]
    percentage: uinteger,
    /// Mandatory title of the progress operation. Used to briefly inform about the kind of operation being performed.
    ///
    /// Examples: "Indexing" or "Linking dependencies".
    title: []const u8,
};

pub const WorkDoneProgressCancelParams = struct {
    /// The token to be used to report progress.
    token: ProgressToken,
};

pub const WorkDoneProgressCreateParams = struct {
    /// The token to be used to report progress.
    token: ProgressToken,
};

pub const WorkDoneProgressEnd = struct {
    kind: []const u8,
    /// Optional, a final message indicating to for example indicate the outcome of the operation.
    message: []const u8,
};

pub const WorkDoneProgressOptions = struct {
    workDoneProgress: bool,
};

pub const WorkDoneProgressParams = struct {
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const WorkDoneProgressReport = struct {
    /// Controls enablement state of a cancel button. This property is only valid if a cancel button got requested in the `WorkDoneProgressBegin` payload.
    ///
    /// Clients that don't support cancellation or don't support control the button's enablement state are allowed to ignore the setting.
    cancellable: bool,
    kind: []const u8,
    /// Optional, more detailed associated progress message. Contains complementary information to the `title`.
    ///
    /// Examples: "3/25 files", "project/src/module2", "node_modules/some_dep". If unset, the previous progress message (if any) is still valid.
    message: []const u8,
    /// Optional progress percentage to display (value 100 is considered 100%). If not provided infinite progress is assumed and clients are allowed to ignore the `percentage` value in subsequent in report notifications.
    ///
    /// The value should be steadily rising. Clients are free to ignore values that are not following this rule. The value range is [0, 100]
    percentage: uinteger,
};

/// Parameters of the workspace diagnostic request.
pub const WorkspaceDiagnosticParams = struct {
    /// The additional identifier provided during registration.
    identifier: []const u8,
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// The currently known diagnostic reports with their previous result ids.
    previousResultIds: []PreviousResultId,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

/// A workspace diagnostic report.
pub const WorkspaceDiagnosticReport = struct {
    items: []WorkspaceDocumentDiagnosticReport,
};

/// A partial result for a workspace diagnostic report.
pub const WorkspaceDiagnosticReportPartialResult = struct {
    items: []WorkspaceDocumentDiagnosticReport,
};

/// A workspace diagnostic document report.
pub const WorkspaceDocumentDiagnosticReport = union(enum) {
    modify_0: WorkspaceFullDocumentDiagnosticReport,
    modify_1: WorkspaceUnchangedDocumentDiagnosticReport,
};

pub const WorkspaceEdit = struct {
    /// A map of change annotations that can be referenced in `AnnotatedTextEdit`s or create, rename and delete file / folder operations.
    ///
    /// Whether clients honor this property depends on the client capability `workspace.changeAnnotationSupport`.
    changeAnnotations: void,
    /// Holds changes to existing resources.
    changes: void,
    /// Depending on the client capability `workspace.workspaceEdit.resourceOperations` document changes are either an array of `TextDocumentEdit`s to express changes to n different text documents where each text document edit addresses a specific version of a text document. Or it can contain above `TextDocumentEdit`s mixed with create, rename and delete file / folder operations.
    ///
    /// Whether a client supports versioned document edits is expressed via `workspace.workspaceEdit.documentChanges` client capability.
    ///
    /// If a client neither supports `documentChanges` nor `workspace.workspaceEdit.resourceOperations` then only plain `TextEdit`s using the `changes` property are supported.
    documentChanges: union(enum) {
        modify_0: []TextDocumentEdit,
        modify_1: []union(enum) {
            modify_0: TextDocumentEdit,
            modify_1: CreateFile,
            modify_2: RenameFile,
            modify_3: DeleteFile,
        },
    },
};

pub const WorkspaceEditClientCapabilities = struct {
    /// Whether the client in general supports change annotations on text edits, create file, rename file and delete file changes.
    changeAnnotationSupport: struct {
        /// Whether the client groups edits with equal labels into tree nodes, for instance all edits labelled with "Changes in Strings" would be a tree node.
        groupsOnLabel: bool,
    },
    /// The client supports versioned document changes in `WorkspaceEdit`s
    documentChanges: bool,
    /// The failure handling strategy of a client if applying the workspace edit fails.
    failureHandling: FailureHandlingKind,
    /// Whether the client normalizes line endings to the client specific setting. If set to `true` the client will normalize line ending characters in a workspace edit to the client specific new line character(s).
    normalizesLineEndings: bool,
    /// The resource operations the client supports. Clients should at least support 'create', 'rename' and 'delete' files and folders.
    resourceOperations: []ResourceOperationKind,
};

pub const WorkspaceFolder = struct {
    /// * The name of the workspace folder. Used to refer to this   * workspace folder in the user interface.
    name: []const u8,
    /// * The associated URI for this workspace folder.
    uri: URI,
};

/// * The workspace folder change event.
pub const WorkspaceFoldersChangeEvent = struct {
    /// * The array of added workspace folders
    added: []WorkspaceFolder,
    /// * The array of the removed workspace folders
    removed: []WorkspaceFolder,
};

pub const WorkspaceFoldersServerCapabilities = struct {
    /// * Whether the server wants to receive workspace folder   * change notifications.      *        * If a string is provided, the string is treated as an ID          * under which the notification is registered on the client            * side. The ID can be used to unregister for these events              * using the `client/unregisterCapability` request.
    changeNotifications: union(enum) { stringboolean },
    /// * The server has support for workspace folders
    supported: bool,
};

/// A full document diagnostic report for a workspace diagnostic result.
pub const WorkspaceFullDocumentDiagnosticReport = struct {
    /// The actual items.
    items: []Diagnostic,
    /// A full document diagnostic report.
    kind: DocumentDiagnosticReportKind,
    /// An optional result id. If provided it will be sent on the next diagnostic request for the same document.
    resultId: []const u8,
    /// The URI for which diagnostic information is reported.
    uri: DocumentUri,
    /// The version number for which the diagnostics are reported. If the document is not marked as open `null` can be provided.
    version: ?union(enum) {
        modify_0: integer,
    },
};

/// * A special workspace symbol that supports locations without a range  *   * @since 3.17.0
pub const WorkspaceSymbol = struct {
    /// * The name of the symbol containing this symbol. This information is for    * user interface purposes (e.g. to render a qualifier in the user interface      * if necessary). It can't be used to re-infer a hierarchy for the document        * symbols.
    containerName: []const u8,
    /// * A data entry field that is preserved on a workspace symbol between a   * workspace symbol request and a workspace symbol resolve request.
    data: LSPAny,
    /// * The kind of this symbol.
    kind: SymbolKind,
    /// * The location of this symbol. Whether a server is allowed to   * return a location without a range depends on the client     * capability `workspace.symbol.resolveSupport`.       *         * See also `SymbolInformation.location`.
    location: union(enum) {
        modify_0: Location,
        modify_1: struct {
            uri: DocumentUri,
        },
    },
    /// * The name of this symbol.
    name: []const u8,
    /// * Tags for this completion item.
    tags: []SymbolTag,
};

pub const WorkspaceSymbolClientCapabilities = struct {
    /// * Symbol request supports dynamic registration.
    dynamicRegistration: bool,
    /// * The client support partial workspace symbols. The client will send the   * request `workspaceSymbol/resolve` to the server to resolve additional     * properties.       *         * @since 3.17.0 - proposedState
    resolveSupport: struct {
        /// * The properties that a client can resolve lazily. Usually     * `location.range`
        properties: [][]const u8,
    },
    /// * Specific capabilities for the `SymbolKind` in the `workspace/symbol`   * request.
    symbolKind: struct {
        /// * The symbol kind values the client supports. When this      * property exists the client also guarantees that it will          * handle values outside its set gracefully and falls back              * to a default value when unknown.                  *                      * If this property is not present the client only supports                          * the symbol kinds from `File` to `Array` as defined in                              * the initial version of the protocol.
        valueSet: []SymbolKind,
    },
    /// * The client supports tags on `SymbolInformation` and `WorkspaceSymbol`.    * Clients supporting tags have to handle unknown tags gracefully.      *        * @since 3.16.0
    tagSupport: struct {
        /// * The tags supported by the client.
        valueSet: []SymbolTag,
    },
};

pub const WorkspaceSymbolOptions = struct {
    /// * The server provides support to resolve additional   * information for a workspace symbol.     *       * @since 3.17.0
    resolveProvider: bool,
    workDoneProgress: bool,
};

/// * The parameters of a Workspace Symbol Request.
pub const WorkspaceSymbolParams = struct {
    /// An optional token that a server can use to report partial results (e.g. streaming) to the client.
    partialResultToken: ProgressToken,
    /// * A query string to filter symbols by. Clients may send an empty    * string here to request all symbols.
    query: []const u8,
    /// An optional token that a server can use to report work done progress.
    workDoneToken: ProgressToken,
};

pub const WorkspaceSymbolRegistrationOptions = struct {
    /// * The server provides support to resolve additional   * information for a workspace symbol.     *       * @since 3.17.0
    resolveProvider: bool,
    workDoneProgress: bool,
};

/// An unchanged document diagnostic report for a workspace diagnostic result.
pub const WorkspaceUnchangedDocumentDiagnosticReport = struct {
    /// A document diagnostic report indicating no changes to the last result. A server can only return `unchanged` if result ids are provided.
    kind: DocumentDiagnosticReportKind,
    /// A result id which will be sent on the next diagnostic request for the same document.
    resultId: []const u8,
    /// The URI for which diagnostic information is reported.
    uri: DocumentUri,
    /// The version number for which the diagnostics are reported. If the document is not marked as open `null` can be provided.
    version: ?union(enum) {
        modify_0: integer,
    },
};

/// Defines a decimal number. Since decimal numbers are very rare in the language server specification we denote the exact range with every decimal using the mathematics interval notation (e.g. [0, 1] denotes all decimals d with 0 <= d <= 1.
pub const decimal = f32;

/// Defines an integer number in the range of -2^31 to 2^31 - 1.
pub const integer = i32;

/// Defines an unsigned integer number in the range of 0 to 2^31 - 1.
pub const uinteger = u32;
