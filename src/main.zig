const std = @import("std");
const ChildProcess = std.ChildProcess;

const core = @import("core");
const imgui = @import("imgui");

const lsp = @import("lsp.zig");
const JsonMessage = lsp.JsonMessage;

const here = core.utils.here;
const log = std.log.scoped(.lsp);

pub const State = @import("state.zig");
pub const Client = @import("client.zig");

pub fn init() !*State {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    var lsp_state = try gpa.allocator().create(State);
    lsp_state.* = .{ .gpa = gpa };
    _ = try core.getHooks().createAndAttach("buffer_created", lsp_state, startServer);
    _ = try core.getHooks().createAndAttach("buffer_created", lsp_state, addBufferToServer);

    _ = try core.getHooks().createAndAttach("after_change", lsp_state, didChange);
    core.addUserUI(@ptrCast(lsp_state), debugUI);
    return lsp_state;
}

fn toState(ptr: *anyopaque) *State {
    return @ptrCast(@alignCast(ptr));
}

pub fn startServer(ptr: *anyopaque, buffer: *core.Buffer) void {
    if (buffer.metadata.buffer_type.len == 0) return;
    var lsp_state = toState(ptr);
    const server_info = lsp_state.serverInfoOf(buffer.metadata.buffer_type) orelse return;
    lsp_state.startServer(server_info) catch |err| log.info("{s}: failed to start [{s}] {!}", .{ here(@src()), server_info.name() orelse "", err });
    lsp_state.addAllBuffersToServers(&core.state().buffers);
}

pub fn addBufferToServer(ptr: *anyopaque, buffer: *core.Buffer) void {
    var lsp_state = toState(ptr);
    lsp_state.didOpen(buffer) catch |err| {
        switch (err) {
            error.ServerUninitialized => startServer(ptr, buffer), // startServer already calls didOpen
            else => log.info("{s}: failed to add buffer to server {!}", .{ here(@src()), err }),
        }
    };
}

pub fn didChange(ptr: *anyopaque, buffer: *core.Buffer, change: core.Buffer.Change) void {
    var lsp_state = toState(ptr);
    lsp_state.didChange(buffer, change) catch return;
}

pub fn debugUI(ptr: ?*anyopaque, arena: std.mem.Allocator) void {
    var lsp_state = toState(ptr orelse return);
    _ = imgui.begin("LSP logs", .{});
    defer imgui.end();

    _ = imgui.beginTabBar("LSP Servers/Clients", .{});
    defer imgui.endTabBar();

    var iter = lsp_state.clients.iterator();
    while (iter.next()) |kv| {
        const name = arena.dupeZ(u8, kv.key_ptr.*) catch return;

        const client = kv.value_ptr.*;
        if (imgui.beginTabItem(name, .{})) {
            defer imgui.endTabItem();

            var size = imgui.getWindowSize();
            {
                const static = struct {
                    pub var buf: [10_000:0]u8 = undefined;
                };
                _ = imgui.inputTextMultiline("Filter", .{ .buf = &static.buf });

                _ = imgui.beginChild("Client", .{ .w = size[0] / 2, .flags = .{ .horizontal_scrollbar = true } });
                defer imgui.endChild();
                if (client.*.client_log.items.len > 0) {
                    const len = std.mem.len(@as([*:0]u8, &static.buf));
                    var content = std.mem.splitScalar(u8, client.*.client_log.items, '\n');

                    outer: while (content.next()) |line| {
                        var filter = std.mem.splitScalar(u8, static.buf[0..len], '\n');
                        while (filter.next()) |f| {
                            if (f.len == 0) continue;
                            if (std.mem.indexOf(u8, line, f) != null) continue :outer;
                        }

                        imgui.textUnformatted(line);
                    }
                }
            }

            imgui.sameLine(.{});

            {
                _ = imgui.beginChild("Server", .{ .w = size[0] / 2, .flags = .{ .horizontal_scrollbar = true } });
                defer imgui.endChild();
                if (client.*.server_log.items.len > 0) imgui.textUnformatted(client.*.server_log.items);
            }
        }
    }
}

pub fn main() !void {}

pub const interfaces = struct {
    pub fn declaration(state_ptr: *State) core.input.MappingSystem.FunctionInterface {
        const declarationFn = struct {
            fn f(ptr: ?*anyopaque) void {
                var state = toState(ptr orelse return);
                var focused = core.focusedBufferAndBW() orelse return;
                var point = focused.bw.data.cursorPoint() orelse return;
                var result = state.declaration(state.gpa.allocator(), focused.buffer, point) catch return;
                defer result.deinit(state.gpa.allocator());
                switch (result) {
                    .locations => |locs| for (locs) |loc| std.debug.print("{} {s}\n", .{ loc.range, loc.uri }),
                    .links => |locs| for (locs) |loc| std.debug.print("{} {s}\n", .{ loc.targetRange, loc.targetUri }),
                }
            }
        }.f;

        return .{ .ptr = @ptrCast(state_ptr), .callFn = declarationFn };
    }
};
