const std = @import("std");
const ChildProcess = std.ChildProcess;

const imgui = @import("imgui");

const lsp = @import("lsp.zig");
const methods = @import("lsp_methods.zig");
const JsonMessage = lsp.JsonMessage;

const Client = @import("client.zig");
const ServerInfo = Client.ServerInfo;

const core = @import("core");
const BufferMap = std.AutoHashMapUnmanaged(core.BufferHandle, core.Buffer);

pub const State = @This();

pub const Error = error{
    NamelessServer,
    ServerDoesNotExist,
    AddingNamelessServer,
    NoServerForBufferType,
    ServerUninitialized,
};

gpa: std.heap.GeneralPurposeAllocator(.{}) = .{},
clients: std.StringHashMapUnmanaged(*Client) = .{}, // has to be a pointer because for some god damn reason the array list fields get reset

pub fn deinit(self: *State) void {
    var iter = self.clients.iterator();
    while (iter.next()) |kv| {
        kv.value_ptr.*.deinit();
        self.gpa.allocator().destroy(kv.value_ptr.*);
    }
    self.clients.deinit(self.gpa.allocator());

    var gpa = self.gpa;
    gpa.allocator().destroy(self);
    _ = gpa.deinit();
}

pub fn startServer(self: *State, server_info: ServerInfo) !void {
    const name = server_info.name() orelse return Error.NamelessServer;
    var client = self.clients.get(name) orelse return Error.ServerDoesNotExist;

    try client.startServer(server_info.init_params);
}

pub fn addServer(self: *State, server_info: ServerInfo) !void {
    const name = server_info.name() orelse return Error.AddingNamelessServer;
    var gop = try self.clients.getOrPut(self.gpa.allocator(), name);
    if (!gop.found_existing) {
        var client = try self.gpa.allocator().create(Client);
        client.* = Client.init(self.gpa.allocator(), server_info);
        gop.value_ptr.* = client;
    }
}

pub fn serverInfoOf(self: *State, buffer_type: []const u8) ?ServerInfo {
    var iter = self.clients.valueIterator();
    while (iter.next()) |client|
        for (client.*.server_info.buffer_types) |bt|
            if (std.mem.eql(u8, buffer_type, bt)) return client.*.server_info;

    return null;
}

pub fn addAllBuffersToServers(self: *State, buffers: *const BufferMap) void {
    var iter = buffers.iterator();
    while (iter.next()) |kv| {
        if (kv.value_ptr.metadata.buffer_type.len == 0) continue;
        self.didOpen(kv.value_ptr) catch |err|
            std.log.err("{s}(): failed to add buffer [{s}] to server {!}\n", .{ @src().fn_name, kv.value_ptr.metadata.file_path, err });
    }
}

pub fn getClient(self: *State, server_info: ServerInfo) !?*Client {
    const name = server_info.name() orelse return Error.NamelessServer;
    var client = self.clients.get(name);

    if (client) |c| switch (c.server_status) {
        .uninitialized, .initializing => return Error.ServerUninitialized,
        .initialized => return c,
    };

    return null;
}

pub fn getClientForBuffer(self: *State, buffer: *const core.Buffer) !*Client {
    const server_info = self.serverInfoOf(buffer.metadata.buffer_type) orelse return Error.NoServerForBufferType;
    var client = (try self.getClient(server_info)) orelse return Error.ServerDoesNotExist;
    return client;
}

////////////////////////////////////////////////////////////////////////////////
// Section: Method wrappers
// These methods should figure out the proper server for the buffers type. As well
// as translating the editor's data types into the lsp data type if needed
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// Section: Notifications
////////////////////////////////////////////////////////////////////////////////

pub fn didOpen(self: *State, buffer: *core.Buffer) !void {
    if (buffer.metadata.buffer_type.len == 0) return;
    const server_info = self.serverInfoOf(buffer.metadata.buffer_type) orelse return Error.NoServerForBufferType;
    var client = (try self.getClient(server_info)) orelse return;

    if (client.server_status == .initialized) {
        const buffer_text = try buffer.getAllLines(self.gpa.allocator());
        defer self.gpa.allocator().free(buffer_text);
        try client.didOpen(server_info.language, buffer.metadata.file_path, buffer_text);
    }
}

pub fn didClose(self: *State, buffer: *core.Buffer) !void {
    if (buffer.metadata.buffer_type.len == 0) return;
    var client = try self.getClientForBuffer(buffer);
    try client.didClose(buffer.metadata.file_path);
}

pub fn didChange(self: *State, buffer: *core.Buffer, change: core.Buffer.Change) !void {
    var client = try self.getClientForBuffer(buffer);

    var changes = std.BoundedArray(lsp.TextDocumentContentChangeEvent, 2).init(0) catch unreachable;
    const change_kind = change.kind();
    // delete must always be first

    const start_point = buffer.getPoint(change.start_index);
    if (change_kind == .delete or change_kind == .replace) {
        const end_index = change.start_index + change.delete_len;
        const range = core.Buffer.PointRange{ .start = start_point, .end = buffer.getPoint(end_index) };
        changes.append(.{ .change = .{ .range = lsp.Range.lsp(range), .text = "" } }) catch unreachable;
    }

    var text: ?[]u8 = null;
    if (change_kind == .insert or change_kind == .replace) {
        const end_index = change.start_index + change.inserted_len;
        text = try buffer.getStringRange(self.gpa.allocator(), .{ .start = change.start_index, .end = end_index });
        changes.append(.{ .change = .{ .range = lsp.Range.lsp(.{ .start = start_point, .end = start_point }), .text = text.? } }) catch unreachable;
    }
    defer if (text) |txt| self.gpa.allocator().free(txt);

    try client.didChange(buffer.metadata.file_path, @intCast(buffer.version), changes.slice());
}

////////////////////////////////////////////////////////////////////////////////
// Section: Requests
////////////////////////////////////////////////////////////////////////////////

pub fn declaration(self: *State, allocator: std.mem.Allocator, buffer: *const core.Buffer, point: core.Buffer.Point) !Client.Declaration {
    var client = try self.getClientForBuffer(buffer);
    return client.declaration(allocator, @truncate(point.row), @truncate(point.col), buffer.metadata.file_path);
}
