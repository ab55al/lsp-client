const std = @import("std");
const json = std.json;

const tres = @import("tres.zig");
const lsp = @import("lsp.zig");

pub const Client = @This();

const file_scheme = "file://";

allocator: std.mem.Allocator,
server: std.ChildProcess,
server_info: ServerInfo,
next_request_id: i32 = 0,
server_status: enum { uninitialized, initializing, initialized } = .uninitialized,
stringify_options: json.StringifyOptions = .{ .emit_null_optional_fields = false },

responses: std.ArrayListUnmanaged(lsp.JsonTree) = .{},
notifications: std.ArrayListUnmanaged(lsp.JsonTree) = .{},
server_requests: std.ArrayListUnmanaged(lsp.JsonTree) = .{},
client_log: std.ArrayListUnmanaged(u8) = .{},
server_log: std.ArrayListUnmanaged(u8) = .{},
logging: bool = @import("builtin").mode == .Debug,

pub const ServerInfo = struct {
    language: []const u8,
    buffer_types: []const []const u8,
    argv: []const []const u8,
    init_params: lsp.InitializeParams = .{},

    pub fn name(self: ServerInfo) ?[]const u8 {
        return if (self.argv.len > 0) self.argv[0] else null;
    }
};

pub fn init(allocator: std.mem.Allocator, server_info: ServerInfo) Client {
    var server = std.ChildProcess.init(server_info.argv, allocator);
    server.stdin_behavior = .Pipe;
    server.stdout_behavior = .Pipe;
    server.stderr_behavior = .Pipe;
    return .{ .allocator = allocator, .server = server, .server_info = server_info };
}

pub fn deinit(client: *Client) void {
    for (client.responses.items) |*tree| tree.deinit(client.allocator);
    for (client.notifications.items) |*tree| tree.deinit(client.allocator);
    for (client.server_requests.items) |*tree| tree.deinit(client.allocator);

    client.responses.deinit(client.allocator);
    client.notifications.deinit(client.allocator);
    client.server_requests.deinit(client.allocator);

    client.server_log.deinit(client.allocator);
    client.client_log.deinit(client.allocator);
}

pub fn startServer(client: *Client, init_params: lsp.InitializeParams) !void {
    if (client.server_status == .initialized) return;

    try client.server.spawn();

    var resp = try client.requestWait(lsp.methods.initialize, init_params, lsp.InitializeResult);
    defer resp.deinit(client.allocator);

    var return_value: union(enum) { ok, err: anyerror } = if (resp.result) |result| switch (result) {
        .result => blk: {
            client.server_status = .initialized;
            break :blk .ok;
        },
        .err => |err| .{ .err = lsp.errors.get(err.message) orelse lsp.Error.UnknownServerError },
    } else .ok;

    if (return_value == .ok) {
        try client.notify(lsp.methods.initialized, lsp.InitializedParams{});
    } else {
        _ = try client.server.kill();
        return return_value.err;
    }
}

pub fn request(client: *Client, method: []const u8, request_value: anytype) !i32 {
    if (!std.mem.eql(u8, method, lsp.methods.initialize) and client.server_status != .initialized) return error.ServerUninitialized;
    var writer = client.server.stdin.?.writer();
    const id = client.newRequestId();
    const req = lsp.Request(@TypeOf(request_value)){
        .id = .{ .integer = id },
        .method = method,
        .params = request_value,
    };
    var message = try JsonMessage.create(client.allocator, req, client.stringify_options);
    defer message.free(client.allocator);
    _ = try message.write(writer);

    client.logRequest(method, request_value);

    return id;
}

pub fn notify(client: *Client, comptime method: []const u8, notification_value: anytype) !void {
    if (client.server_status != .initialized) return error.ServerUninitialized;
    const notification = lsp.Notification(@TypeOf(notification_value)){
        .method = method,
        .params = notification_value,
    };

    const message = try JsonMessage.create(client.allocator, notification, client.stringify_options);
    defer message.free(client.allocator);
    _ = try message.write(client.server.stdin.?.writer());

    client.logNotification(method, notification_value);
}

pub fn response(client: *Client, method: []const u8, request_id: i32, comptime ResponseType: type) !lsp.Response(ResponseType) {
    if (!std.mem.eql(u8, method, lsp.methods.initialize) and client.server_status != .initialized) return error.ServerUninitialized;
    @setEvalBranchQuota(5000);

    try client.readMessages();
    var tree = blk: {
        for (client.responses.items, 0..) |message, i| {
            const id = (message.parsed.value.object.get("id") orelse continue).integer;
            if (id == request_id)
                break :blk client.responses.swapRemove(i);
        }

        return error.NoResponse;
    };
    errdefer tree.deinit(client.allocator);

    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    errdefer arena.deinit();

    var result: ?ResponseType = blk: {
        var result = tree.parsed.value.object.get("result") orelse break :blk null;
        if (result == .null) break :blk null;
        break :blk try tres.parse(ResponseType, result, arena.allocator());
    };

    var err: ?lsp.ResponseError = blk: {
        var err = tree.parsed.value.object.get("error") orelse break :blk null;
        if (err == .null) break :blk null;
        break :blk try tres.parse(lsp.ResponseError, err, arena.allocator());
    };

    if (result) |res|
        client.logResponse(method, res)
    else if (err) |e|
        client.logResponse(method, e)
    else
        client.logResponse(method, null);

    return lsp.Response(ResponseType){
        .jsonrpc = "2.0",
        .id = if (tree.parsed.value.object.get("id")) |id| switch (id) {
            .integer => |int| .{ .integer = @truncate(int) },
            .string => |str| .{ .string = str },
            else => .null,
        } else .null,

        .result = if (result) |res| .{ .result = res } else if (err) |e| .{ .err = e } else null,

        .tree = tree,
        .arena = arena,
    };
}

pub fn requestWait(client: *Client, method: []const u8, request_value: anytype, comptime ResponseType: type) !lsp.Response(ResponseType) {
    var id = try client.request(method, request_value);
    return client.response(method, id, ResponseType);
}

pub fn readMessages(client: *Client) !void {
    while (client.messageExists(500)) {
        var stdout = client.server.stdout.?;
        var reader = stdout.reader();
        var header = try Header.readAndParse(client.allocator, reader);
        defer header.maybeFreeType(client.allocator);
        var message_content = try readAll(client.allocator, header, reader);
        errdefer client.allocator.free(message_content);

        var parsed = try json.parseFromSlice(json.Value, client.allocator, message_content, .{});
        errdefer parsed.deinit();

        const has_id = parsed.value.object.get("id") != null;
        const has_method = parsed.value.object.get("method") != null;

        if (has_id and !has_method)
            try client.responses.append(client.allocator, .{ .raw = message_content, .parsed = parsed })
        else if (has_method and !has_id)
            try client.notifications.append(client.allocator, .{ .raw = message_content, .parsed = parsed })
        else if (has_id and has_method)
            try client.server_requests.append(client.allocator, .{ .raw = message_content, .parsed = parsed })
        else {
            std.log.warn("Unknown message {s}", .{message_content});
            client.allocator.free(message_content);
            parsed.deinit();
        }
    }

    while (client.logExists(5)) {
        var reader = client.server.stderr.?.reader();
        while (true) {
            var byte = reader.readByte() catch break;
            try client.server_log.append(client.allocator, byte);
            if (!client.logExists(50)) break;
        }
    }
}

pub fn logNotification(client: *Client, method: []const u8, args: anytype) void {
    if (!client.logging) return;
    var writer = client.client_log.writer(client.allocator);
    writer.print("Sent notification [{s}] ", .{method}) catch return;
    print(writer, "", args);
    writer.print("\n", .{}) catch return;
}

pub fn logNotificationFromServer(client: *Client, method: []const u8, args: anytype) void {
    if (!client.logging) return;
    var writer = client.client_log.writer(client.allocator);
    writer.print("Received notification [{s}] ", .{method}) catch return;
    print(writer, "", args);
    writer.print("\n", .{}) catch return;
}

pub fn logRequest(client: *Client, method: []const u8, args: anytype) void {
    if (!client.logging) return;

    var writer = client.client_log.writer(client.allocator);
    writer.print("Sent request [{s}] ", .{method}) catch return;
    print(writer, "", args);
    writer.print("\n", .{}) catch return;
}

pub fn logResponse(client: *Client, method: []const u8, args: anytype) void {
    if (!client.logging) return;

    var writer = client.client_log.writer(client.allocator);
    writer.print("Received response [{s}] ", .{method}) catch return;
    print(writer, "", args);
    writer.print("\n", .{}) catch return;
}

fn readAll(allocator: std.mem.Allocator, header: Header, reader: anytype) ![]const u8 {
    var list = std.ArrayList(u8).init(allocator);
    try list.ensureTotalCapacity(header.content_len);
    defer list.deinit();

    while (list.items.len < header.content_len) {
        var byte = reader.readByte() catch break;
        try list.append(byte);
    }

    return list.toOwnedSlice();
}

pub const Header = struct {
    content_len: usize,
    content_type: ?[]const u8,

    pub fn maybeFreeType(header: Header, allocator: std.mem.Allocator) void {
        if (header.content_type) |t| allocator.free(t);
    }

    pub fn read(allocator: std.mem.Allocator, reader: anytype) usize {
        _ = reader;
        _ = allocator;
    }

    pub fn readAndParse(allocator: std.mem.Allocator, reader: anytype) !Header {
        var list = std.ArrayList(u8).init(allocator);
        errdefer list.deinit();

        const Content_Length = "Content-Length: ";
        const content_length_max = Content_Length.len + std.fmt.count("{}", .{std.math.maxInt(u64)});
        try reader.readUntilDelimiterArrayList(&list, '\n', content_length_max);
        const length_str = list.items;
        _ = std.mem.indexOf(u8, length_str, Content_Length) orelse return error.NoContentLength;
        var extracted = extractInt(length_str) orelse return error.NoContentLength;
        var parsed_len = try std.fmt.parseInt(usize, extracted, 10);

        // reset the array list
        list.resize(0) catch unreachable;

        const Content_Type = "Content-Type: ";
        try reader.readUntilDelimiterArrayList(&list, '\n', std.math.maxInt(u16));
        const type_value: ?[]u8 = if (std.mem.indexOf(u8, list.items, Content_Type) == null) null else try list.toOwnedSlice();

        if (type_value == null) list.deinit();

        return .{
            .content_len = parsed_len,
            .content_type = type_value,
        };
    }
};

pub const JsonMessage = struct {
    header: []const u8,
    content: []const u8,

    pub fn create(allocator: std.mem.Allocator, value: anytype, options: json.StringifyOptions) !JsonMessage {
        var content = blk: {
            var list = std.ArrayList(u8).init(allocator);
            defer list.deinit();
            try json.stringify(value, options, list.writer());
            break :blk try list.toOwnedSlice();
        };

        const header = blk: {
            const header_fmt = "Content-Length: {}\r\n\r\n";
            break :blk try std.fmt.allocPrint(allocator, header_fmt, .{content.len});
        };
        return .{ .header = header, .content = content };
    }

    pub fn write(message: JsonMessage, writer: anytype) !usize {
        var bytes: usize = 0;
        bytes += try writer.write(message.header);
        bytes += try writer.write(message.content);
        return bytes;
    }

    pub fn free(message: JsonMessage, allocator: std.mem.Allocator) void {
        allocator.free(message.header);
        allocator.free(message.content);
    }
};

fn extractInt(string: []const u8) ?[]const u8 {
    var start: ?usize = null;

    inline for (0..10) |num| {
        const needle = std.fmt.digitToChar(num, .lower);
        var index = std.mem.indexOf(u8, string, &.{needle});
        if (index) |i| {
            if (start) |s| start = @min(i, s) else start = i;
        }
    }

    const s = start orelse return null;
    var end = s;
    for (string[s..]) |c| {
        if (std.ascii.isDigit(c)) end += 1 else break;
    }

    return string[s..end];
}

fn print(writer: anytype, name: []const u8, value: anytype) void {
    const type_info = @typeInfo(@TypeOf(value));

    switch (type_info) {
        .Struct => {
            const fields = comptime std.meta.fields(@TypeOf(value));
            if (name.len > 0)
                writer.print(".{s} {{ ", .{name}) catch return
            else
                writer.print("{{ ", .{}) catch return;

            inline for (fields) |f| print(writer, f.name, @field(value, f.name));
            writer.print("}} ", .{}) catch return;
        },
        .Optional => {
            if (value == null)
                writer.print("[{s} null] ", .{name}) catch return
            else if (value) |v|
                print(writer, name, v);
        },
        .Union => {
            switch (value) {
                inline else => |v| print(writer, "", v),
            }
        },
        .Pointer => |P| {
            if (P.size == .Slice) {
                if (P.child == u8) {
                    if (std.mem.count(u8, value, "\n") > 0) {
                        writer.print("[{s} \"...\"] ", .{name}) catch return;
                    } else {
                        writer.print("[{s} \"{s}\"] ", .{ name, value }) catch return;
                    }
                } else if (P.child == []const u8 or P.child == []u8) {
                    writer.print("[{s} ", .{name}) catch return;

                    for (value) |v| {
                        if (std.mem.count(u8, v, "\n") > 0)
                            writer.print("\"...\", ", .{}) catch return
                        else
                            writer.print("\"{s}\", ", .{v}) catch return;
                    }

                    writer.print("]", .{}) catch return;
                } else {
                    for (value) |v| print(writer, "", v);
                }
            }
        },
        else => {
            writer.print("[{s} {any}] ", .{ name, value }) catch return;
        },
    }
}

fn newRequestId(client: *Client) i32 {
    const id = client.next_request_id;
    client.next_request_id += 1;
    return id;
}

fn messageExists(client: *Client, timeout: i32) bool {
    var poller = std.io.poll(client.allocator, enum { stdout }, .{
        .stdout = client.server.stdout.?,
    });
    defer poller.deinit();

    const events_len = std.os.poll(&poller.poll_fds, timeout) catch return false;
    return events_len > 0;
}

fn logExists(client: *Client, timeout: i32) bool {
    var poller = std.io.poll(client.allocator, enum { stderr }, .{
        .stderr = client.server.stderr.?,
    });
    defer poller.deinit();

    const events_len = std.os.poll(&poller.poll_fds, timeout) catch return false;
    return events_len > 0;
}

fn fileUriFromPath(allocator: std.mem.Allocator, path: []const u8) ![]u8 {
    if (std.mem.indexOf(u8, path, file_scheme) != null) {
        return allocator.dupe(u8, path);
    } else {
        var buf = try allocator.alloc(u8, file_scheme.len + path.len);
        std.mem.copyForwards(u8, buf, file_scheme);
        std.mem.copyForwards(u8, buf[file_scheme.len..], path);
        return buf;
    }
}

///////////////////////////////////////////////////////////////////////////////
// Section: Notification wrappers
///////////////////////////////////////////////////////////////////////////////

pub fn didOpen(client: *Client, language: []const u8, path: []const u8, text: []const u8) !void {
    const uri = try fileUriFromPath(client.allocator, path);
    defer client.allocator.free(uri);

    try client.notify(lsp.methods.textDocument.didOpen, lsp.DidOpenTextDocumentParams{
        .textDocument = lsp.TextDocumentItem{
            .uri = uri,
            .languageId = language,
            .version = 0,
            .text = text,
        },
    });
}

pub fn didChange(client: *Client, path: []const u8, version: i32, changes: []const lsp.TextDocumentContentChangeEvent) !void {
    const uri = try fileUriFromPath(client.allocator, path);
    defer client.allocator.free(uri);

    try client.notify(lsp.methods.textDocument.didChange, lsp.DidChangeTextDocumentParams{
        .textDocument = .{ .uri = uri, .version = version },
        .contentChanges = changes,
    });
}

pub fn didClose(client: *Client, path: []const u8) !void {
    const uri = try fileUriFromPath(client.allocator, path);
    defer client.allocator.free(uri);

    try client.notify(lsp.methods.textDocument.didClose, lsp.DidCloseTextDocumentParams{
        .textDocument = .{ .uri = uri },
    });
}

///////////////////////////////////////////////////////////////////////////////
// Section: Request wrappers
///////////////////////////////////////////////////////////////////////////////

pub const Declaration = union(enum) {
    locations: []lsp.Location,
    links: []lsp.LocationLink,

    pub fn deinit(self: Declaration, allocator: std.mem.Allocator) void {
        switch (self) {
            .locations => |locs| {
                for (locs) |loc| allocator.free(loc.uri);
                allocator.free(locs);
            },

            .links => |links| {
                for (links) |link| allocator.free(link.targetUri);
                allocator.free(links);
            },
        }
    }
};
pub fn declaration(client: *Client, allocator: std.mem.Allocator, line: u32, character: u32, path: []const u8) !Declaration {
    const uri = try fileUriFromPath(client.allocator, path);
    defer client.allocator.free(uri);
    var decl_params = lsp.DeclarationParams{
        .textDocument = .{ .uri = uri },
        .position = .{ .line = line, .character = character },
    };

    var resp = try client.requestWait(lsp.methods.textDocument.declaration, decl_params, lsp.DeclarationParams.Response);
    defer resp.deinit(client.allocator);

    if (resp.result) |res| switch (res) {
        .err => |err| {
            return lsp.errors.get(err.message) orelse {
                std.log.err("Unknown error message: [{s}]", .{err.message});
                return error.UnknownServerError;
            };
        },
        .result => |server_result| {
            if (server_result.result) |full_result| switch (full_result) {
                .loc => |loc| return .{ .locations = try lsp.Location.dupeS(allocator, &.{loc}) },
                .locs => |v| return .{ .locations = try lsp.Location.dupeS(allocator, v) },
                .loc_links => |v| return .{ .links = try lsp.LocationLink.dupeS(allocator, v) },
            };
            if (server_result.part_result) |part_result| switch (part_result) {
                .locs => |v| return .{ .locations = try lsp.Location.dupeS(allocator, v) },
                .loc_links => |v| return .{ .links = try lsp.LocationLink.dupeS(allocator, v) },
            };
        },
    };

    return error.NoResponse;
}
