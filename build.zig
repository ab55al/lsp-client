const std = @import("std");

inline fn thisDir() []const u8 {
    return comptime std.fs.path.dirname(@src().file) orelse ".";
}

pub fn module(bob: *std.Build) *std.Build.Module {
    return bob.createModule(.{ .source_file = .{ .path = thisDir() ++ "/src/main.zig" } });
}

pub fn build(bob: *std.Build) void {
    const target = bob.standardTargetOptions(.{});
    const optimize = bob.standardOptimizeOption(.{});

    const exe = bob.addExecutable(.{
        .name = "thabit-lsp-client",
        .root_source_file = .{ .path = thisDir() ++ "/src/main.zig" },
        .target = target,
        .optimize = optimize,
    });

    bob.installArtifact(exe);

    const run_cmd = bob.addRunArtifact(exe);
    run_cmd.step.dependOn(bob.getInstallStep());

    const run_step = bob.step("run", "Run");
    run_step.dependOn(&run_cmd.step);
}
